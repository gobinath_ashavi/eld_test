<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class CommonEmailModel extends CI_Model {

    public function __construct() {
        parent::__construct();
        
		date_default_timezone_set("Asia/Kolkata");
    }

    /*** Get Email Template and Send Mail to Corresponding User ***/
	// Paramters List -> template_name ( Required ), subject ( Not Mandatory ), user_name ( Not Mandatory ), user_id ( Not Mandatory ), custom_url ( Not Mandatory )
    function getSendEmail($data) {
		//echo '<pre>'; print_r($data);
		if(isset($data['template_name'])) {
			$cond = array(
				'title' => $data['template_name'],
				'method' => 'email',
				'status' => 1
			);
			$subject = isset($data['subject']) ? $data['subject'] : '';
			$site_url = get_site_url();
			$site_name = 'E-Learning';
			$user_name = isset($data['user_name']) ? $data['user_name'] : '';
			$custom_url = isset($data['custom_url']) ? $data['custom_url'] : '';			
			$receiver_email = isset($data['email']) ? $data['email'] : '';
			
			//echo $site_url . ' +++ ' . $custom_url . ' +++ ' . $receiver_email . ' +++ ' . $user_name . ' <br/> ';
			
			$email_template = $this->db->select('*')->from('templates')->where($cond)->get()->row_array();
			
			if(isset($data['user_id'])) {
				$user_cond = array(
					'id' => isset($data['user_id']) ? $data['user_id'] : 0
				);
				
				$get_user_details = $this->db->select('*')->from('users')->where($user_cond)->get()->row_array();
				
				$user_name = isset($get_user_details['first_name']) ? $get_user_details['first_name'] . ($get_user_details['last_name'] ? ' ' . $get_user_details['last_name'] : '') : '';
			
				$receiver_email = isset($get_user_details['email']) ? $get_user_details['email'] : '';
			}
			
			$subject = isset($email_template['subject']) ? $email_template['subject'] : '';
			//$template = $email_template['template'];
			$template = htmlspecialchars($email_template['template']);
			
			$template = str_replace('{{e.subject}}', $subject, $template);
			$template = str_replace('{{e.site_url}}', $site_url, $template);
			$template = str_replace('{{e.user}}', $user_name, $template);
			$template = str_replace('{{e.activeurl}}', $custom_url, $template);
			$template = str_replace('{{e.site_name}}', $site_name, $template);
			
			echo '<pre>'; print_r($template);
			//print_r($email_template); 
			exit;
		} else
			return false;
    }
}