<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class CommonModel extends CI_Model {

    public function __construct() {
        parent::__construct();
        
		date_default_timezone_set("Asia/Kolkata");
    }

    /*** Get single row / all data from corresponding table ***/
    function getRawQueryResult($sql){
        return $this->db->query($sql)->result_array();
    }

        function getRawQueryResult_single($sql){
        return $this->db->query($sql)->row_array();
    }
    /*** Get single row / all data from corresponding table ***/
    function fetchData($table_name, $field, $cond, $limit, $flag, $order = ""){
    	$field_name = !empty($field) ? $field : '*';
		
		$this->db->select($field_name);
        $this->db->from($table_name);
        
        if(!empty($cond))
			$this->db->where($cond);
		
		if(!empty($limit)) {
            if(is_array($limit))
                $this->db->limit($limit['length'], $limit['start']);
            else
                $this->db->limit($limit);
        }

		if(!empty($order))
			$this->db->order_by($order);
        
		//flag = 0 => single row; flag = 1 => all rows
		if($flag == 0) {
			//$result = $this->db->get_where($table_name, $cond)->row_array();
			$query = $this->db->get();
            $result = ($query->num_rows() > 0) ? $query->row_array() : FALSE;
		} else {
			$query = $this->db->get();
			$result = ($query->num_rows() > 0) ? $query->result_array() : FALSE;
		}

		return $result;
    }
    
    /*** Insert data into corresponding table ***/
    public function insert($data, $table_name) {
        // Add status, created at and updated at if not exists
		/*if(!array_key_exists("status", $data)){
            $data['status'] = 1;
        }
        if(!array_key_exists("created", $data)){
            $data['created_at'] = date("Y-m-d H:i:s");
        }
        if(!array_key_exists("modified", $data)){
            $data['updated_at'] = date("Y-m-d H:i:s");
        }*/
        
        // Insert data into corresponding table
        $insert = $this->db->insert($table_name, $data);
        
        return $insert ? $this->db->insert_id() : FALSE;
    }
    
    /*** Update data into corresponding table ***/
    public function update($data, $cond, $table_name) {
		// Check whether corresponded id have rows
		$this->db->select('id');
        $this->db->from($table_name);
		if(!empty($cond))
			$this->db->where($cond);
		$query = $this->db->get();
		$result = ($query->num_rows() > 0) ? TRUE : FALSE;

        // Update data into corresponding table
		if($result)
			$update = $this->db->update($table_name, $data, $cond);
		else
			$update = FALSE;
		
        return $update ? TRUE : FALSE;
    }

    /*** Delete data from corresponding table ***/
    public function delete($cond, $table_name) {
        // Check whether corresponded id have rows
        $this->db->select('id');
        $this->db->from($table_name);
        if(!empty($cond))
            $this->db->where($cond);
        $query = $this->db->get();
        $result = ($query->num_rows() > 0) ? TRUE : FALSE;

        // Update data into corresponding table
        if($result)
            $delete = $this->db->delete($table_name, $cond);
        else
            $delete = FALSE;
        
        return $delete ? TRUE : FALSE;
    }
	
	/*** Get total data count from corresponding table ***/
    function getTotalCount($table_name, $field, $cond){
		$field_name = !empty($field) ? $field : '*';
		
		$this->db->select($field_name);
        $this->db->from($table_name);
        
        if(!empty($cond))
			$this->db->where($cond);
		
		$query = $this->db->get();
		$totalCnt = $query->num_rows();

        return $totalCnt;
    }


      public function assign_module($user, $modules)
    {
      
        $data = array('modules' => $modules);

        $this->db->where('id', $user);
        $this->db->update('users', $data);

        return true;

    }




        public function mod_assign($mod){
        $this->db->select('modules');
        $this->db->from('users');
        $this->db->where('id',$mod);
        $query = $this->db->get();
        $data = $query->row_array();
        $users = json_decode($data['modules']);
//         echo $this->db->last_query();
// print_r($users);
// exit();
        //$users = (explode(',', $data));
        if ($users) {
            foreach ($users as $key) {
                $this->db->select("*");
                $this->db->from('modules');
                $this->db->where('id',$key);
                $query = $this->db->get();
                $data1[] = $query->row_array();
            }
        return $data1;  
        }
    }
}
