<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ValidateSession
{
	var $CI;
    public function __construct() 
	{
        $this->CI =& get_instance();
		$this->CI->load->library('session');
    }

    public function validate()
    {
		$controllerName = $this->CI->uri->segment(1);
		$methodName = $this->CI->uri->segment(2);
		$urlArray = array('signin', 'signup', 'authenticate', 'checkUserEmailForForgotPassword', 'resetPassword', 'updateResetPassword', 'logout');
		if(!in_array($methodName, $urlArray)) {
			if(!$this->CI->session->userdata('session_user_details')['id']) {
				/*if(!$this->CI->session->userdata('admin')['id'] || !$this->CI->session->userdata('company')['id'] || !$this->CI->session->userdata('user')['id']) {
					redirect('auth/signin');
					exit();
				}*/
				redirect('auth/signin');
				exit();
			}
		}
    }
}