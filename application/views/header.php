<header class="topbar">

	<link href="<?php echo base_url() ?>template/assets/node_modules/toast-master/css/jquery.toast.css"
		  rel="stylesheet">

	<link href="<?php echo base_url() ?>template/assets/node_modules/sweetalert/sweetalert.css" rel="stylesheet">


	<link rel="stylesheet" href="<?php echo base_url() ?>template/dist/css/jquery.multiselect.css" type="text/css">
	<style type="text/css">
		ul, li {
			margin: 0;
			padding: 0;
			list-style: none;
		}

		.label {
			color: #000;
			font-size: 16px;
		}

		.ms-options-wrap {
			position: inherit;
		}
	</style>


	<nav class="navbar top-navbar navbar-expand-md navbar-dark">

		<!-- ============================================================== -->

		<!-- Logo -->

		<!-- ============================================================== -->

		<div class="navbar-header">

			<a class="navbar-brand" href="<?php echo base_url() ?>">

				<!-- Logo icon --><b>

					<!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->

					<!-- Dark Logo icon -->

					<img src="<?php echo base_url() ?>template/assets/images/logo-icon.png" alt="homepage"
						 class="dark-logo"/>

					<!-- Light Logo icon -->

					<!-- <img src="<?php echo base_url() ?>template/assets/images/logo-light-icon.png" alt="homepage" class="light-logo" /> -->

				</b>

				<!--End Logo icon -->

				<!-- Logo text --><span class="hidden-sm-down">

                         <!-- dark Logo text -->

                         <img src="<?php echo base_url() ?>template/assets/images/logo-text.png" alt="homepage"
							  class="dark-logo"/>

					<!-- Light Logo text -->

                         <img style="height: 50px;border-radius: 10px"
							  src="<?php echo base_url() ?>template/assets/images/logo.jpg" class="light-logo"
							  alt="homepage"/></span> </a>

		</div>

		<!-- ============================================================== -->

		<!-- End Logo -->

		<!-- ============================================================== -->

		<div class="navbar-collapse">

			<!-- ============================================================== -->

			<!-- toggle and nav items -->

			<!-- ============================================================== -->

			<ul class="navbar-nav mr-auto">

				<!-- This is  -->

				<li class="nav-item"><a class="nav-link nav-toggler d-block d-sm-none waves-effect waves-dark"
										href="javascript:void(0)"><i class="ti-menu"></i></a></li>

				<li class="nav-item"><a class="nav-link sidebartoggler d-none waves-effect waves-dark"
										href="javascript:void(0)"><i class="icon-menu"></i></a></li>

				<!-- ============================================================== -->

				<!-- Search -->

				<!-- ============================================================== -->

				<!--   <li class="nav-item">

					  <form class="app-search d-none d-md-block d-lg-block">

						  <input type="text" class="form-control" placeholder="Search & enter">

					  </form>

				  </li> -->

			</ul>

			<!-- ============================================================== -->

			<!-- User profile and search -->

			<!-- ============================================================== -->

			<ul class="navbar-nav my-lg-0">

				<!-- ============================================================== -->

				<!-- Comment -->

				<!-- ============================================================== -->

				<li style="display: none" class="nav-item dropdown">

					<a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown"
					   aria-haspopup="true" aria-expanded="false"> <i class="ti-email"></i>

						<div class="notify"><span class="heartbit"></span> <span class="point"></span></div>

					</a>

					<div class="dropdown-menu dropdown-menu-right mailbox animated bounceInDown">

						<ul>

							<li>

								<div class="drop-title">Notifications</div>

							</li>

							<li>

								<div class="message-center">

									<!-- Message -->

									<a href="javascript:void(0)">

										<div class="btn btn-danger btn-circle"><i class="fa fa-link"></i></div>

										<div class="mail-contnet">

											<h5>Luanch Admin</h5> <span
													class="mail-desc">Just see the my new admin!</span> <span
													class="time">9:30 AM</span></div>

									</a>

									<!-- Message -->

									<a href="javascript:void(0)">

										<div class="btn btn-success btn-circle"><i class="ti-calendar"></i></div>

										<div class="mail-contnet">

											<h5>Event today</h5> <span class="mail-desc">Just a reminder that you have event</span>
											<span class="time">9:10 AM</span></div>

									</a>

									<!-- Message -->

									<a href="javascript:void(0)">

										<div class="btn btn-info btn-circle"><i class="ti-settings"></i></div>

										<div class="mail-contnet">

											<h5>Settings</h5> <span class="mail-desc">You can customize this template as you want</span>
											<span class="time">9:08 AM</span></div>

									</a>

									<!-- Message -->

									<a href="javascript:void(0)">

										<div class="btn btn-primary btn-circle"><i class="ti-user"></i></div>

										<div class="mail-contnet">

											<h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span>
											<span class="time">9:02 AM</span></div>

									</a>

								</div>

							</li>

							<li>

								<a class="nav-link text-center link" href="javascript:void(0);"> <strong>Check all
										notifications</strong> <i class="fa fa-angle-right"></i> </a>

							</li>

						</ul>

					</div>

				</li>

				<!-- ============================================================== -->

				<!-- End Comment -->

				<!-- ============================================================== -->

				<!-- ============================================================== -->

				<!-- Messages -->

				<!-- ============================================================== -->

				<li style="display: none;" class="nav-item dropdown">

					<a class="nav-link dropdown-toggle waves-effect waves-dark" href="" id="2" data-toggle="dropdown"
					   aria-haspopup="true" aria-expanded="false"> <i class="icon-note"></i>

						<div class="notify"><span class="heartbit"></span> <span class="point"></span></div>

					</a>

					<div class="dropdown-menu mailbox dropdown-menu-right animated bounceInDown" aria-labelledby="2">

						<ul>

							<li>

								<div class="drop-title">You have 4 new messages</div>

							</li>

							<li>

								<div class="message-center">

									<!-- Message -->

									<a href="javascript:void(0)">

										<div class="user-img"><img
													src="<?php echo base_url() ?>template/assets/images/users/1.jpg"
													alt="user" class="img-circle"> <span
													class="profile-status online pull-right"></span></div>

										<div class="mail-contnet">

											<h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span>
											<span class="time">9:30 AM</span></div>

									</a>

									<!-- Message -->

									<a href="javascript:void(0)">

										<div class="user-img"><img
													src="<?php echo base_url() ?>template/assets/images/users/2.jpg"
													alt="user" class="img-circle"> <span
													class="profile-status busy pull-right"></span></div>

										<div class="mail-contnet">

											<h5>Sonu Nigam</h5> <span
													class="mail-desc">I've sung a song! See you at</span> <span
													class="time">9:10 AM</span></div>

									</a>

									<!-- Message -->

									<a href="javascript:void(0)">

										<div class="user-img"><img
													src="<?php echo base_url() ?>template/assets/images/users/3.jpg"
													alt="user" class="img-circle"> <span
													class="profile-status away pull-right"></span></div>

										<div class="mail-contnet">

											<h5>Arijit Sinh</h5> <span class="mail-desc">I am a singer!</span> <span
													class="time">9:08 AM</span></div>

									</a>

									<!-- Message -->

									<a href="javascript:void(0)">

										<div class="user-img"><img
													src="<?php echo base_url() ?>template/assets/images/users/4.jpg"
													alt="user" class="img-circle"> <span
													class="profile-status offline pull-right"></span></div>

										<div class="mail-contnet">

											<h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span>
											<span class="time">9:02 AM</span></div>

									</a>

								</div>

							</li>

							<li>

								<a class="nav-link text-center link" href="javascript:void(0);"> <strong>See all
										e-Mails</strong> <i class="fa fa-angle-right"></i> </a>

							</li>

						</ul>

					</div>

				</li>

				<!-- ============================================================== -->

				<!-- End Messages -->

				<!-- ============================================================== -->

				<!-- ============================================================== -->

				<!-- mega menu -->

				<!-- ============================================================== -->

				<li style="display: none;" class="nav-item dropdown mega-dropdown"><a
							class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown"
							aria-haspopup="true" aria-expanded="false"><i class="ti-layout-width-default"></i></a>

					<div class="dropdown-menu animated bounceInDown">

						<ul class="mega-dropdown-menu row">

							<li class="col-lg-3 col-xlg-2 m-b-30">

								<h4 class="m-b-20">CAROUSEL</h4>


								<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">

									<div class="carousel-inner" role="listbox">

										<div class="carousel-item active">

											<div class="container"><img class="d-block img-fluid"
																		src="<?php echo base_url() ?>template/assets/images/big/img1.jpg"
																		alt="First slide"></div>

										</div>

										<div class="carousel-item">

											<div class="container"><img class="d-block img-fluid"
																		src="<?php echo base_url() ?>template/assets/images/big/img2.jpg"
																		alt="Second slide"></div>

										</div>

										<div class="carousel-item">

											<div class="container"><img class="d-block img-fluid"
																		src="<?php echo base_url() ?>template/assets/images/big/img3.jpg"
																		alt="Third slide"></div>

										</div>

									</div>

									<a class="carousel-control-prev" href="#carouselExampleControls" role="button"
									   data-slide="prev"> <span class="carousel-control-prev-icon"
																aria-hidden="true"></span> <span class="sr-only">Previous</span>
									</a>

									<a class="carousel-control-next" href="#carouselExampleControls" role="button"
									   data-slide="next"> <span class="carousel-control-next-icon"
																aria-hidden="true"></span> <span
												class="sr-only">Next</span> </a>

								</div>


							</li>

							<li class="col-lg-3 m-b-30">

								<h4 class="m-b-20">ACCORDION</h4>


								<div id="accordion" class="nav-accordion" role="tablist" aria-multiselectable="true">

									<div class="card">

										<div class="card-header" role="tab" id="headingOne">

											<h5 class="mb-0">

												<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
												   aria-expanded="true" aria-controls="collapseOne">

													Collapsible Group Item #1

												</a>

											</h5></div>

										<div id="collapseOne" class="collapse show" role="tabpanel"
											 aria-labelledby="headingOne">

											<div class="card-body"> Anim pariatur cliche reprehenderit, enim eiusmod
												high.
											</div>

										</div>

									</div>

									<div class="card">

										<div class="card-header" role="tab" id="headingTwo">

											<h5 class="mb-0">

												<a class="collapsed" data-toggle="collapse" data-parent="#accordion"
												   href="#collapseTwo" aria-expanded="false"
												   aria-controls="collapseTwo">

													Collapsible Group Item #2

												</a>

											</h5></div>

										<div id="collapseTwo" class="collapse" role="tabpanel"
											 aria-labelledby="headingTwo">

											<div class="card-body"> Anim pariatur cliche reprehenderit, enim eiusmod
												high life accusamus terry richardson ad squid.
											</div>

										</div>

									</div>

									<div class="card">

										<div class="card-header" role="tab" id="headingThree">

											<h5 class="mb-0">

												<a class="collapsed" data-toggle="collapse" data-parent="#accordion"
												   href="#collapseThree" aria-expanded="false"
												   aria-controls="collapseThree">

													Collapsible Group Item #3

												</a>

											</h5></div>

										<div id="collapseThree" class="collapse" role="tabpanel"
											 aria-labelledby="headingThree">

											<div class="card-body"> Anim pariatur cliche reprehenderit, enim eiusmod
												high life accusamus terry richardson ad squid.
											</div>

										</div>

									</div>

								</div>

							</li>

							<li class="col-lg-3  m-b-30">

								<h4 class="m-b-20">CONTACT US</h4>


								<form>

									<div class="form-group">

										<input type="text" class="form-control" id="exampleInputname1"
											   placeholder="Enter Name"></div>

									<div class="form-group">

										<input type="email" class="form-control" placeholder="Enter email"></div>

									<div class="form-group">

										<textarea class="form-control" id="exampleTextarea" rows="3"
												  placeholder="Message"></textarea>

									</div>

									<button type="submit" class="btn btn-info">Submit</button>

								</form>

							</li>

							<li class="col-lg-3 col-xlg-4 m-b-30">

								<h4 class="m-b-20">List style</h4>

								<!-- List style -->

								<ul class="list-style-none">

									<li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> You can
											give link</a></li>

									<li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> Give link</a>
									</li>

									<li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> Another
											Give link</a></li>

									<li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> Forth link</a>
									</li>

									<li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> Another
											fifth link</a></li>

								</ul>

							</li>

						</ul>

					</div>

				</li>

				<!-- ============================================================== -->

				<!-- End mega menu -->

				<!-- ============================================================== -->

				<!-- ============================================================== -->

				<!-- User Profile -->

				<!-- ============================================================== -->

				<li class="nav-item dropdown u-pro">

					<a class="nav-link dropdown-toggle waves-effect waves-dark profile-pic" href=""
					   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img
								src="<?php echo base_url() ?>template/assets/images/users/usericon.PNG" alt="user"
								class=""> <span
								class="hidden-md-down"><?php echo $this->session->userdata('session_dtls')['user_name']; ?> &nbsp;<i
									class="fa fa-angle-down"></i></span> </a>

					<div class="dropdown-menu dropdown-menu-right animated flipInY">

						<!-- text-->

						<!-- <a href="javascript:void(0)" class="dropdown-item"><i class="ti-user"></i> My Profile</a> -->

						<!-- text-->

						<!-- <a href="javascript:void(0)" class="dropdown-item"><i class="ti-wallet"></i> My Balance</a> -->

						<!-- text-->

						<!-- <a href="javascript:void(0)" class="dropdown-item"><i class="ti-email"></i> Inbox</a> -->

						<!-- text-->

						<!-- <div class="dropdown-divider"></div> -->

						<!-- text-->

						<!-- <a href="javascript:void(0)" class="dropdown-item"><i class="ti-settings"></i> Account Setting</a> -->

						<!-- text-->

						<!-- <div class="dropdown-divider"></div> -->

						<!-- text-->

						<a href="<?php echo base_url() ?>auth/logout" class="dropdown-item"><i
									class="fa fa-power-off"></i> Logout</a>

						<!-- text-->

					</div>

				</li>

				<!-- ============================================================== -->

				<!-- End User Profile -->

				<!-- ============================================================== -->

				<li style="display: none;" class="nav-item right-side-toggle"><a
							class="nav-link  waves-effect waves-light" href="javascript:void(0)"><i
								class="ti-settings"></i></a></li>

			</ul>

		</div>

	</nav>

</header>

<!-- ============================================================== -->

<!-- End Topbar header -->

<!-- ============================================================== -->

<!-- ============================================================== -->

<!-- Left Sidebar - style you can find in sidebar.scss  -->

<!-- ============================================================== -->

<aside class="left-sidebar">

	<!-- Sidebar scroll-->

	<div class="scroll-sidebar">

		<!-- Sidebar navigation-->

		<nav class="sidebar-nav">

			<ul id="sidebarnav">

				<li class="user-pro"><a class="has-arrow waves-effect waves-dark" href="javascript:void(0)"
										aria-expanded="false"><img
								src="<?php echo base_url() ?>template/assets/images/users/1.jpg" alt="user-img"
								class="img-circle"><span class="hide-menu">Mark Jeckson</span></a>

					<ul aria-expanded="false" class="collapse">

						<li><a href="javascript:void(0)"><i class="ti-user"></i> My Profile</a></li>

						<li><a href="javascript:void(0)"><i class="ti-wallet"></i> My Balance</a></li>

						<li><a href="javascript:void(0)"><i class="ti-email"></i> Inbox</a></li>

						<li><a href="javascript:void(0)"><i class="ti-settings"></i> Account Setting</a></li>

						<li><a href="javascript:void(0)"><i class="fa fa-power-off"></i> Logout</a></li>

					</ul>

				</li>

				<li class="nav-small-cap">--- PERSONAL</li>

				<li><a class="" href="<?php echo base_url() ?>" aria-expanded="false"><i
								class="icon-speedometer"></i><span class="hide-menu">Home <span
									class="badge badge-pill badge-cyan ml-auto">4</span></span></a>
					<!--
                            <ul aria-expanded="false" class="collapse">

                                <li><a href="<?php echo base_url() ?>">Dashboard </a></li>

                            </ul> -->

				</li>


				<li class="nav-small-cap">--- FORMS, TABLE &amp; WIDGETS</li>


				<li class="nav-small-cap">--- EXTRA COMPONENTS</li>


				<?php

				if ($this->session->userdata('session_dtls')['role'] == '1') {

					?>

					<li><a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i
									class="ti-align-left"></i><span class="hide-menu">Users</span></a>

						<ul aria-expanded="false" class="collapse">

							<li><a href="<?php echo base_url() ?>users/add">Add</a></li>

							<li><a href="<?php echo base_url() ?>users/view">View</a></li>


						</ul>

					</li>

					<li><a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i
									class="ti-align-left"></i><span class="hide-menu">Modules</span></a>

						<ul aria-expanded="false" class="collapse">

							<li><a href="<?php echo base_url() ?>modules/add">Add</a></li>

							<li><a href="<?php echo base_url() ?>modules/view">View</a></li>

							<li><a href="<?php echo base_url() ?>admin/assign_module">Assign</a></li>

							<!--<li><a href="<?php //echo base_url() ?>modules/content/view">Content</a></li>-->

						</ul>

					</li>

					<li><a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i
									class="ti-align-left"></i><span class="hide-menu">Questions</span></a>
						<ul aria-expanded="false" class="collapse">
							<li><a href="<?php echo base_url() ?>questions/add">Add</a></li>
							<li><a href="<?php echo base_url() ?>questions/view">View</a></li>
						</ul>
					</li>

					<li><a href="<?php echo base_url() ?>admin/orientation"><span
									class="hide-menu">Orientation</span></a>

					</li>

				<?php } ?>


				<?php
				if ($this->session->userdata('session_dtls')['role'] == '3') {
					?>

					<li><a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i
									class="ti-align-left"></i><span class="hide-menu">Exam History</span></a>
						<ul aria-expanded="false" class="collapse">
							<li><a href="<?php echo base_url() ?>student/exam_history/pending">Pending</a></li>
							<li><a href="<?php echo base_url() ?>student/exam_history/completed">Completed</a></li>
						</ul>
					</li>
				<?php } ?>

			</ul>

		</nav>

		<!-- End Sidebar navigation -->

	</div>

	<!-- End Sidebar scroll-->

</aside>
