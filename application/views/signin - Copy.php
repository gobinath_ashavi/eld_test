<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Tell the browser to be responsive to screen width -->

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">

    <meta name="author" content="">

    <!-- Favicon icon -->

    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url()?>template/assets/images/favicon.png">

    <title>E-Learning | Signin</title>

    

    <!-- page css -->

    <link href="<?php echo base_url()?>template/dist/css/pages/login-register-lock.css" rel="stylesheet">

    <!-- Custom CSS -->

    <link href="<?php echo base_url()?>template/dist/css/style.min.css" rel="stylesheet">

    <link href="<?php echo base_url()?>template/assets/node_modules/toast-master/css/jquery.toast.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

<![endif]-->

<style>
    /*
Template Name: Admin Template
Author: Wrappixel

File: scss
*/
@import url(https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700);
/*Theme Colors*/
/**
 * Table Of Content
 *
 *  1. Color system
 *  2. Options
 *  3. Body
 *  4. Typography
 *  5. Breadcrumbs
 *  6. Cards
 *  7. Dropdowns
 *  8. Buttons
 *  9. Typography
 *  10. Progress bars
 *  11. Tables
 *  12. Forms
 *  14. Component
 */
/*******************
Login register and recover password Page
******************/
.login-register {
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center center;
  height: 100%;
  width: 100%;
  padding: 10% 0;
  position: fixed; }

.login-box {
  width: 400px;
  margin: 0 auto; }
  .login-box .footer {
    width: 100%;
    left: 0px;
    right: 0px; }
  .login-box .social {
    display: block;
    margin-bottom: 30px; }

#recoverform {
  display: none; }

.login-sidebar {
  padding: 0px;
  margin-top: 0px; }
  .login-sidebar .login-box {
    right: 0px;
    position: absolute;
    height: 100%; }


    .login-register{

        position: absolute;

    }

</style>

</head>



<body style="height: 1000px; background:url(<?php echo base_url('template/assets/images/pattern2.jpg')?>)" class="horizontal-nav boxed skin-megna card-no-border">

    <!-- ============================================================== -->

    <!-- Preloader - style you can find in spinners.css -->

    <!-- ============================================================== -->

    <div class="preloader">

        <div class="loader">

            <div class="loader__figure"></div>

            <p class="loader__label">Elite admin</p>

        </div>

    </div>

    <!-- ============================================================== -->

    <!-- Main wrapper - style you can find in pages.scss -->

    <!-- ============================================================== -->

    <section id="wrapper">

        <!-- <div class="login-register" style="background-image:url(<?php echo base_url()?>template/assets/images/background/login-register.jpg);"> -->

            <div class="login-register">

            <div class="login-box card">

                <div class="card-body">

                    <form class="form-horizontal form-material" id="loginform" >

                        <h2 style="text-align: center;">E-Learning</h2>





                        <?php 

// if($this->session->userdata('session_dtls')){

//      // do something when exist

//     echo "session seted";

// }else{

//     echo "faildd to set";

//     // do something when doesn't exist

// }

?>

                        <h3 style="text-align: center;" class="box-title m-b-20">Sign In</h3>

                        <div class="form-group ">

                            <div class="col-xs-12">

                                <input class="form-control" type="text" name="email" required="" placeholder="Username"> </div>

                        </div>

                        <div class="form-group">

                            <div class="col-xs-12">

                                <input class="form-control" type="password" name="password" required="" placeholder="Password"> </div>

                        </div>

                        <div class="form-group row">

                            <div class="col-md-12">

                                <div class="custom-control custom-checkbox">

                                    <input type="checkbox" class="custom-control-input" id="customCheck1">

                                    <label class="custom-control-label" for="customCheck1">Remember me</label>

                                    <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> Forgot pwd?</a> 

                                </div> 

                            </div>

                        </div>

                        <div class="form-group text-center">

                            <div class="col-xs-12 p-b-20">

                                <button class="btn btn-block btn-lg btn-info btn-rounded login_button" type="submit">Log In</button>

                            </div>

                        </div>

           <!--              <div class="row">

                            <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">

                                <div class="social">

                                    <a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip" title="Login with Facebook"> <i aria-hidden="true" class="fa fa-facebook"></i> </a>

                                    <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip" title="Login with Google"> <i aria-hidden="true" class="fa fa-google-plus"></i> </a>

                                </div>

                            </div>

                        </div> -->

                        <div class="form-group m-b-0">

                            <div class="col-sm-12 text-center">

                                Don't have an account? <a href="<?php echo base_url()?>auth/signup" class="text-info m-l-5"><b>Sign Up</b></a>

                            </div>

                        </div>

                    </form>

                    <form class="form-horizontal" id="recoverform" action="index.html">

                        <div class="form-group ">

                            <div class="col-xs-12">

                                <h3>Recover Password</h3>

                                <p class="text-muted">Enter your Email and instructions will be sent to you! </p>

                            </div>

                        </div>

                        <div class="form-group ">

                            <div class="col-xs-12">

                                <input class="form-control" type="text" required="" placeholder="Email"> </div>

                        </div>

                        <div class="form-group text-center m-t-20">

                            <div class="col-xs-12">

                                <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Reset</button>

                            </div>

                        </div>

                    </form>

                </div>

            </div>

        </div>

    </section>

    

    <!-- ============================================================== -->

    <!-- End Wrapper -->

    <!-- ============================================================== -->

    <!-- ============================================================== -->

    <!-- All Jquery -->

    <!-- ============================================================== -->

    <script src="<?php echo base_url()?>template/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>

    <!-- Bootstrap tether Core JavaScript -->

    <script src="<?php echo base_url()?>template/assets/node_modules/popper/popper.min.js"></script>

    <script src="<?php echo base_url()?>template/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>

    <script src="<?php echo base_url()?>template/assets/node_modules/toast-master/js/jquery.toast.js"></script>

    <script src="<?php echo base_url()?>template/dist/js/pages/toastr.js"></script>

    <!--Custom JavaScript -->

    <script type="text/javascript">

        $(function() {

            $(".preloader").fadeOut();

        });

        $(function() {

            $('[data-toggle="tooltip"]').tooltip()

        });

        // ============================================================== 

        // Login and Recover Password 

        // ============================================================== 

        $('#to-recover').on("click", function() {

            $("#loginform").slideUp();

            $("#recoverform").fadeIn();

        });





         $(document).ready(function(){



          $("#loginform").submit(function( event ) {



           $(".login_button").html('Please wait');

           $(".login_button").prop("disabled", true);



            event.preventDefault();

            var datas = $('#loginform').serialize();

            $.ajax({

              url:"<?php echo base_url()?>auth/authenticate",

              method:'post',

              data:datas,

              success:function(data){



                var res = jQuery.parseJSON(data);

                // console.log(res.status);

                

                if (res.status == true) {

                    // alert(res.status_text);

                    $(".login_button").html('Login success');

                    $.toast({

                    heading: 'Success',

                    text: res.status_text,

                    position: 'top-right',

                    loaderBg:'#ff6849',

                    icon: 'success',

                    hideAfter: 3500, 

                    stack: 6

                  });

                  // setTimeout(function(){ location.reload(); }, 1000);


                     setTimeout(function(){ 
                  window.location = "<?php echo base_url()."auth/two_step_verification"; ?>";
                  
                }, 1000);





                }else{

                    // alert('failed');



                    $(".login_button").html('Log In');

                    $(".login_button").prop("disabled", false);



                    $.toast({

                    heading: 'Failed',

                    text: res.status_text,

                    position: 'top-right',

                    loaderBg:'#ff6849',

                    icon: 'error',

                    hideAfter: 3500

                  });





                  // setTimeout(function(){ location.reload(); }, 1000);



                }      

              }



            });



          });



        });

    </script>

    





</body>



</html>