<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url()?>template/assets/images/favicon.png">
    <title>E-Learning | Signup</title>
    
    <!-- page css -->
    <link href="<?php echo base_url()?>template/dist/css/pages/login-register-lock.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url()?>template/dist/css/style.min.css" rel="stylesheet">
         <link href="<?php echo base_url()?>template/assets/node_modules/toast-master/css/jquery.toast.css" rel="stylesheet">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<style>
    .login-register{
        position: absolute;
    }
</style>
</head>

<body style="height: 1000px; background:url(<?php echo base_url('template/assets/images/pattern2.jpg')?>)" class="horizontal-nav boxed skin-megna card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Elite admin</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper">
        <div class="login-register">
            <div class="login-box card">
                <div class="card-body">
                    
                     <!-- <form class="form-horizontal form-material" id="loginform" method="POST" action="<?php echo base_url()?>auth/newUser"> -->
                     <form class="form-horizontal form-material" id="regform">
                        <h2 style="text-align: center;">E-Learning</h2>
                        <h3 style="text-align: center;" class="box-title m-b-20">Sign Up</h3>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control" type="text" name="first_name" required="" placeholder="First Name">
                            </div>
                        </div>
                          <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control" type="text" name="last_name" required="" placeholder="Last Name">
                            </div>
                        </div>

                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input class="form-control" name="email" type="email" required="" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input class="form-control" type="password" name="password" required="" placeholder="Password">
                            </div>
                        </div>
                   
                    <!--     <div class="form-group row">
                            <div class="col-md-12">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                                    <label class="custom-control-label" for="customCheck1">I agree to all <a href="javascript:void(0)">Terms</a></label> 
                                </div> 
                            </div>
                        </div> -->
                        <div class="form-group text-center p-b-20">
                            <div class="col-xs-12">
                                <button class="btn btn-info btn-lg btn-block btn-rounded text-uppercase waves-effect waves-light login_button" type="submit">Sign Up</button>
                            </div>
                        </div>
                        <div class="form-group m-b-0">
                            <div class="col-sm-12 text-center">
                                Already have an account? <a href="<?php echo base_url()?>auth/signin" class="text-info m-l-5"><b>Sign In</b></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url()?>template/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>


    <script type="text/javascript">
        
        $(document).ready(function(){

          $("#regform").submit(function( event ) {

            event.preventDefault();
            var datas = $('#regform').serialize();
            $.ajax({
              url:"<?php echo base_url()?>auth/newUser",
              method:'post',
              data:datas,
              success:function(data){
                var res = jQuery.parseJSON(data);
                // console.log(res.status);
                 $(".login_button").attr( { value:"Please Wait ...", disabled:true } );
                if (res.status == true) {
                    // alert(res.status_text);

                    $.toast({
                    heading: 'Success',
                    text: res.status_text,
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                  });
                  // setTimeout(function(){ location.reload(); }, 1000);

                }else{
                         $(".login_button").attr( { value:"Sign UP", disabled:false } );
                    // alert('failed');
                    $.toast({
                    heading: 'Failed',
                    text: res.status_text,
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                  });

                  // setTimeout(function(){ location.reload(); }, 1000);

                }      
              }

            });

          });

        });
    </script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url()?>template/assets/node_modules/popper/popper.min.js"></script>
    <script src="<?php echo base_url()?>template/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>

    <script src="<?php echo base_url()?>template/assets/node_modules/toast-master/js/jquery.toast.js"></script>
    <script src="<?php echo base_url()?>template/dist/js/pages/toastr.js"></script>

    <!--Custom JavaScript -->
    <script type="text/javascript">
        $(function() {
            $(".preloader").fadeOut();
        });
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        });
        // ============================================================== 
        // Login and Recover Password 
        // ============================================================== 
        $('#to-recover').on("click", function() {
            $("#loginform").slideUp();
            $("#recoverform").fadeIn();
        });





    </script>

</body>

</html>