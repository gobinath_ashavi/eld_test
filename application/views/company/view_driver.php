<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo get_project_name_for_browser_tab(); ?> | View Drivers</title>
    <?php include "header.php" ?>
  </head>
  <body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
      <?php include "sidebar.php" ?>
      <style type="text/css">
        .dataTables_filter {
            display: block;
        }
      </style>

      <div class="content-wrapper">
        <div class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1 class="m-0">View Drivers</h1>
              </div>
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url()?>company/home">Dashboard</a></li>
                  <li class="breadcrumb-item active">View Drivers</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <section class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-lg-12 col-12">

                <h3 style="text-align: center;">View Drivers</h3>
                <div class="table-responsive">
                  <div class="form-group row">
                    <div class="col-lg-4">
                      <select class="form-control" id="driver_type" name="driver_type">
                        <option value=""> -- Select Driver Type -- </option>
                        <option value="citydriver">City Driver</option>
                        <option value="canadian_driver">Canadian Driver</option>
                        <option value="us_driver">US Driver</option>
                      </select>
                    </div>
                  </div>

                  <table id="driversList" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Driver ID</th> 
                        <th>Driver Name</th>
                        <th>License No</th>
                        <th>Licence Province</th>
                        <th>CDN Cycle</th>
                        <th>US Cycle</th>
                        <th>Contact Details</th>
                        <th>Terminal</th>
                        <th>Current Location</th>
                        <th>Timezone</th>
                        <th>Status</th>
                        <th class="no-sort" style="width:12%;">Action</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                        <th>#</th>
                        <th>Driver ID</th> 
                        <th>Driver Name</th>
                        <th>License No</th>
                        <th>Licence Province</th>
                        <th>CDN Cycle</th>
                        <th>US Cycle</th>
                        <th>Contact Details</th>
                        <th>Terminal</th>
                        <th>Current Location</th>
                        <th>Timezone</th>
                        <th>Status</th>
                        <th class="no-sort" style="width:12%;">Action</th>
                      </tr>
                    </tfoot>
                  </table>
                </div>

              </div>
            </div>
          </div>
        </section>
      </div>
      <?php include "footer.php" ?>

      <script>
        $(document).ready(function() {
          loadDrivers();          
        });

        $(document).on("change", "#driver_type", function(e) {
          e.stopImmediatePropagation();

          loadDrivers();
        });

        function loadDrivers() {
          $('#driversList').dataTable().fnClearTable();
          $('#driversList').dataTable().fnDestroy();

          $('#driversList')
          .on( 'order.dt',  function () { console.log('ORDER'); } )
          .on( 'search.dt', function () { console.log('SEARCH'); } )
          .on( 'page.dt',   function () { console.log('PAGE'); } )
          .DataTable({
            "sScrollX": "100%",
            "sScrollXInner": "110%",
            "scrollX": true,
            dom: 'Bfrtip',
            buttons: [
              'copy', 'csv', 'excel', 'pdf', 'print'
            ],

            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            "bSort" : false,

            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": true,
            "responsive": false,

            // Load data for the table's content from an Ajax source
            "ajax": {
              //"url": "<?php echo base_url(); ?>company/driver/list",
              "url": "<?php echo base_url(); ?>company/getDriverList",
              "type": "POST",
              "data" : {
                'driver_type' : $('#driver_type').val()
              }
            },
            //Set column definition initialisation properties.
            "columnDefs": [
              {
                "targets": [ 0 ], //first column / numbering column
                "orderable": false, //set not orderable
              },
            ],
          });
        }

        $(document).on('click', '#driversList tbody td a.update_driver_status', function(e) {
          e.stopImmediatePropagation();
          
          var driver_id = $(this).data('driver_id');
          var driver_status = $(this).data('driver_status');
          Swal.fire({
            title: '<div>Confirmation</div><br>',
            text: "Are you sure to " + (driver_status == '1' ? 'delete' : 'active') + " driver details ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false 
          }).then((result) => {
            if (result.value) {
              $.ajax({
                //url: "<?php //echo base_url()?>company/driver/delete",
                url: "<?php echo base_url()?>company/deleteDriver",
                type: "POST",
                dataType: "json",
                data: {
                  "id" : driver_id,
                  "status" : driver_status
                },
                success: function(data) {
                  if(data.status) {
                    Swal.fire(data.status_text, '', 'success');
                    setTimeout(function(){ location.reload(); }, 2000);
                  } else {
                    Swal.fire('Failed to delete driver details', '', 'warning');
                    return false;
                  }
                }
              });
            }
          });
        });
      </script>

    </div>
  </body>
</html>