<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo get_project_name_for_browser_tab(); ?> | Assigned Devices</title>
    <?php include "header.php" ?>
  </head>
  <body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
      <?php include "sidebar.php" ?>
      <style type="text/css">
        .dataTables_filter {
            display: block;
        }
      </style>

      <div class="content-wrapper">
        <div class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1 class="m-0">View Assigned Devices</h1>
              </div>
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url()?>company/home">Dashboard</a></li>
                  <li class="breadcrumb-item active">View Assigned Devices</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <section class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-lg-12 col-12">

                <h3 style="text-align: center;">View Assigned Devices</h3>
                <div class="table-responsive">
                  <div class="form-group row">
                    <div class="col-lg-3">
                      <select class="form-control" id="device_status" name="device_status">
                        <option value=""> -- Select Device Status -- </option>
                        <option value="1">Assign</option>
                        <option value="0">Unassign</option>
                      </select>
                    </div>

                    <div class="col-lg-3">
                      <select class="form-control" id="vehicle_type" name="vehicle_type">
                        <option value=""> -- Select Vehicle Type -- </option>
                        <option value="1">Truck</option>
                        <option value="2">Trailer</option>
                        <option value="0">Other</option>
                      </select>
                    </div>
                  </div>

                  <table id="devicesList" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Device Name</th>
                        <th>Type of Device</th>
                        <th>Assigned To Unit Number</th>
                        <th>Unit Type</th>
                        <th>Plate Number</th>
                        <th>ECM GPS Number</th>
                        <th>IMEI Number</th>
                        <th>ICCID / SIM Number</th>
                        <th>Vehicle Type</th>
                        <th>Status</th>
                        <th class="no-sort" style="width:12%;">Action</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                        <th>#</th>
                        <th>Device Name</th>
                        <th>Type of Device</th>
                        <th>Assigned To Unit Number</th>
                        <th>Unit Type</th>
                        <th>Plate Number</th>
                        <th>ECM GPS Number</th>
                        <th>IMEI Number</th>
                        <th>ICCID / SIM Number</th>
                        <th>Vehicle Type</th>
                        <th>Status</th>
                        <th class="no-sort" style="width:12%;">Action</th>
                      </tr>
                    </tfoot>
                  </table>
                </div>

              </div>
            </div>
          </div>
        </section>
      </div>
      <?php include "footer.php" ?>



      <!-- modal content -->
      <div id="assign_device_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="myModalLabel">Assign Device</h4>
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
              <form class="form-horizontal form-material" id="assign_device_form" method="post">
                <input type="hidden" id="device_id" value="0">

                <div class="form-group row">
                  <label class="col-sm-4">Device Name</label>
                  <div class="col-sm-8">
                    <span id="device_name"></span>
                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-sm-12">
                    <input type="radio" class="device_type" name="device_type" value="1" onclick="showTruck()" checked/>&nbsp;
                    <label for="one">Truck</label>&nbsp;&nbsp;&nbsp;
                    <input type="radio" class="device_type" name="device_type" value="2" onclick="showTrailer()" /> &nbsp;
                    <label for="two">Trailer</label>
                  </div>
                </div>

                <div id="truckh"> 
                  <div class="form-group row">
                    <label class="col-form-label col-sm-4">Truck </label>
                    <div class="col-sm-8">
                      <select class="form-control form-control-line" id="truck_id" name="truck_id">
                        <option value=""> -- Select Truck -- </option>
                        <?php 
                          if(count($truck_detail) > 0) {
                            foreach ($truck_detail as $tk => $tv) {
                              echo "<option value='".$tv['id']."'>" . $tv['unit_no'] . "</option>"; 
                            }
                          }
                        ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div id="trailerh">
                    <div class="form-group row">
                      <label class="col-form-label col-sm-4">Trailer </label>
                      <div class="col-sm-8">
                        <select class="form-control form-control-line" id="trailer_id" name="trailer_id">
                          <option value=""> -- Select Trailer -- </option>
                          <?php 
                            if(count($trailer_detail) > 0) {
                              foreach ($trailer_detail as $tk => $tv) {
                                echo "<option value='".$tv['id']."'>" . $tv['unit_no'] . "</option>"; 
                              }
                            }
                          ?>
                          </select>
                        </div>
                      </div>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button class="btn btn-info" type="button" id="assign_module_info">Submit</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="">Close</button>
              </div>
            </div>
          </div>
        </div>
      </div>


      <script>
        $(document).ready(function() {
          loadAssignedDevices();          
        });

        $(document).on('change', '#device_status, #vehicle_type', function(e) {
          e.stopImmediatePropagation();

          loadAssignedDevices();
        });

        function loadAssignedDevices() {
          $('#devicesList').dataTable().fnClearTable();
          $('#devicesList').dataTable().fnDestroy();

          $('#devicesList')
          .on( 'order.dt',  function () { console.log('ORDER'); } )
          .on( 'search.dt', function () { console.log('SEARCH'); } )
          .on( 'page.dt',   function () { console.log('PAGE'); } )
          .DataTable({
            dom: 'Bfrtip',
            buttons: [
              'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            "bSort" : false,

            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": true,
            "responsive": false,

            // Load data for the table's content from an Ajax source
            "ajax": {
              //"url": "<?php //echo base_url(); ?>company/user/list",
              "url": "<?php echo base_url(); ?>company/getAssignedDeviceList",
              "type": "POST",
              "data" : {
                'device_status' : $("#device_status").val(),
                'vehicle_type' : $("#vehicle_type").val()
              }
            },
            //Set column definition initialisation properties.
            "columnDefs": [
              {
                "targets": [ 0 ], //first column / numbering column
                "orderable": false, //set not orderable
              },
            ],
          });
        }

        $(document).on('click', '#devicesList tbody td a.assign_device', function(e) {
          e.stopImmediatePropagation();

          var device_id = $(this).data('device_id');
          var device_type = $(this).data('device_type');
          if(device_id == "" || device_id == "undefined" || device_id == null) {
            Swal.fire('Device info not found', '', 'warning');
            return false;
          }

          if(device_type > 0) {
            Swal.fire('This device is already assigned', '', 'warning');
            return false;
          }

          Swal.fire({
            title: '<div>Confirmation</div><br>',
            text: "Are you sure to assign the device ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false 
          }).then((result) => {
            if (result.value) {
              $.ajax({
                //url: "<?php //echo base_url()?>company/driver/delete",
                url: "<?php echo base_url()?>company/getDeviceInfo",
                type: "POST",
                dataType: "json",
                data: {
                  "id" : device_id
                },
                success: function(data) {
                  if(data.status) {
                    $('#assign_device_modal #assign_device_form #device_id').val(data.device_info.id);
                    $('#assign_device_modal #assign_device_form #device_name').html(data.device_info.devicename);
                    showTruck();
                    $('#assign_device_modal').modal('show');
                  } else {
                    Swal.fire('Failed to get device details', '', 'warning');
                    return false;
                  }
                }
              });
            }
          });
        });

        $(document).on('click', '#assign_device_modal #assign_module_info', function(e) {
          e.stopImmediatePropagation();

          var device_id = $('#assign_device_modal #assign_device_form #device_id').val();
          var device_type = $("#assign_device_modal #assign_device_form .device_type:checked").val();
          var truck_id = "";
          var trailer_id = "";
          
          if(device_id == "" || device_id == "undefined" || device_id == null) {
            Swal.fire('Device info not found', '', 'warning');
            return false;
          }

          if(parseInt(device_type) == 0) {
            Swal.fire('Select Truck or Trailer', '', 'warning');
            return false;
          }

          if(parseInt(device_type) == 1) {
            truck_id = $('#assign_device_modal #assign_device_form #truck_id').val();
            if(truck_id == "") {
              Swal.fire('Select Truck', '', 'warning');
              return false;   
            }
          }

          if(parseInt(device_type) == 2) {
            trailer_id = $('#assign_device_modal #assign_device_form #trailer_id').val();
            if(trailer_id == "") {
              Swal.fire('Select Trailer', '', 'warning');
              return false;   
            }
          }

          var device_mode = parseInt(device_type) == 1 ? 'truck' : 'trailer';
          if(device_mode == "") {
            Swal.fire('Select Truck or Trailer', '', 'warning');
            return false;
          }

          Swal.fire({
            title: '<div>Confirmation</div><br>',
            text: "Are you sure to assign the device ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false 
          }).then((result) => {
            if (result.value) {
              $.ajax({
                //url: "<?php //echo base_url()?>company/driver/delete",
                url: "<?php echo base_url()?>company/saveAssignDeviceInfo",
                type: "POST",
                dataType: "json",
                data: {
                  "device_id" : device_id,
                  "device_type" : parseInt(device_type),
                  "device_mode" : device_mode,
                  "truck_id" : truck_id,
                  "trailer_id" : trailer_id
                },
                success: function(data) {
                  if(data.status) {
                    $('#assign_device_modal').modal('hide');
                    Swal.fire(data.status_text, '', 'success');
                    setTimeout(function(){ location.reload(); }, 2000);
                  } else {
                    Swal.fire('Failed to get device details', '', 'warning');
                    return false;
                  }
                }
              });
            }
          });
        });

        function showTruck() {
          $("#trailer_id, #truck_id").val("");
          $("#truckh").show();
          $("#trailerh").hide();
        } 

        function showTrailer() {
          $("#trailer_id, #truck_id").val("");
          $("#trailerh").show();
          $("#truckh").hide();
        }

        $(document).on('click', '#devicesList tbody td a.unassign_device', function(e) {
          e.stopImmediatePropagation();

          var device_id = $(this).data('device_id');
          var device_type = $(this).data('device_type');
          var device_mode = $(this).data('device_mode');
          var truck_trailer_id = $(this).data('truck_trailer_id');
          if(device_id == "" || device_id == "undefined" || device_id == null) {
            Swal.fire('Device info not found', '', 'warning');
            return false;
          }

          if(device_type == 0) {
            Swal.fire('This device is not assigned', '', 'warning');
            return false;
          }

          var params = {
            'id' : truck_trailer_id,
            'device_mode' : device_mode,
            'device_id' : device_id,
            'device_type' : device_type
          };

          Swal.fire({
            title: '<div>Confirmation</div><br>',
            text: "Are you sure to unassign the device ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false 
          }).then((result) => {
            if (result.value) {
              $.ajax({
                //url: "<?php //echo base_url()?>company/driver/delete",
                url: "<?php echo base_url()?>company/removeAssignDeviceInfo",
                type: "POST",
                dataType: "json",
                data: params,
                success: function(data) {
                  if(data.status) {
                    Swal.fire(data.status_text, '', 'success');
                    setTimeout(function(){ location.reload(); }, 2000);
                  } else {
                    Swal.fire('Failed to get device details', '', 'warning');
                    return false;
                  }
                }
              });
            }
          });
        });

        function unassignDevice(id) {
          if(id != "" && id != "0" && id != "undefined" && id != null) {
            $.ajax({
              //url: "<?php //echo base_url()?>company/driver/delete",
              url: "<?php echo base_url()?>company/getDeviceInfo",
              type: "POST",
              dataType: "json",
              data: {
                "id" : device_id
              },
              success: function(data) {
                if(data.status) {
                  unassignDevice(data.device_info.id);
                } else {
                  Swal.fire('Failed to get device details', '', 'warning');
                  return false;
                }
              }
            });
          } else {
            Swal.fire('Failed to get device details', '', 'warning');
            return false;
          }
        }
      </script>

    </div>
  </body>
</html>