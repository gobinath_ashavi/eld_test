<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ashaviglobal Company | Add Role</title>

    <?php include "header.php" ?>
  </head>
  <body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
      <?php include "sidebar.php" ?>
      <style type="text/css">
        .dataTables_filter {
            display: block;
        }
      </style>

      <div class="content-wrapper">
        <div class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1 class="m-0">Add Driver</h1>
              </div>
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url()?>company/home">Dashboard</a></li>
                  <li class="breadcrumb-item"><a href="<?php echo base_url()?>company/viewDriver">View driver</a></li>
                  <li class="breadcrumb-item active">Add driver</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <section class="content">
          <div class="container-fluid">
            <div class="row">
               <div class="col-md-2">
               </div>
              <div class="col-md-8">
                <div class=" ">
                <!-- <h3 style="text-align: center;">Add Role</h3> -->
                
              


              <form class="" id="create_driver" >
                                           <div class="form-group row">
                                                <label class="col-form-label col-sm-2">Driver ID <font color="red">*</font></label>
                                                <div class="col-sm-5">
                                                    <input type="text" name="driver_id" id="driver_id"  class="form-control" required>
                                                </div>
                                            </div>
                                            
                                          
                                            <div class="form-group row">
                                                <label class="col-form-label col-sm-2">Username <font color="red">*</font></label>
                                                <div class="col-sm-5">
                                                    <input type="text" name="username" id="username" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-form-label col-sm-2">Password <font color="red">*</font></label>
                                                <div class="col-sm-5">
                                                    <input type="password" name="password" id="password" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-form-label col-sm-2">Confirm Password <font color="red">*</font></label>
                                                <div class="col-sm-5">
                                                    <input type="password" name="cpassword" id="cpassword" class="form-control" onblur="validate_pass()" required>
                                                </div>
                                                <div class="col-sm-5">
                                                     <span id='error_message'></span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-form-label col-sm-2">Driver First Name <font color="red">*</font></label>
                                                <div class="col-sm-5">
                                                    <input type="text" name="name" id="name" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-form-label col-sm-2">Driver Last Name <font color="red">*</font></label>
                                                <div class="col-sm-5">
                                                    <input type="text" name="lastname" id="lastname" class="form-control" required>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <label class="col-form-label col-sm-2">Date of Birth <font color="red">*</font></label>
                                                <div class="col-sm-5">
                                                    <input type="date" name="dob" id="dob" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-form-label col-sm-2">Citizenship <font color="red">*</font></label>
                                                <div class="col-sm-5">
                                                    <input type="text" name="citizenship" id="citizenship" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-form-label col-sm-2">Address <font color="red">*</font></label>
                                                <div class="col-sm-5">
                                                    <input type="text" name="address" id="address" class="form-control" required>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <label class="col-form-label col-sm-2">Postal/Zip Code <font color="red">*</font></label>
                                                <div class="col-sm-5">
                                                    <input type="text" name="postalcode" id="postalcode" class="form-control" required>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <label class="col-form-label col-sm-2">Mobile 1 <font color="red">*</font></label>
                                                <div class="col-sm-5">
                                                    <input type="number" name="phone" id="phone" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-form-label col-sm-2">Mobile 2 </label>
                                                <div class="col-sm-5">
                                                    <input type="number" name="mobile2" id="mobile2" class="form-control" >
                                                </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <label class="col-form-label col-sm-2">Email Id <font color="red">*</font></label>
                                                <div class="col-sm-5">
                                                    <input type="email" name="mailid" id="mailid" class="form-control" required>
                                                </div>
                                            </div>
                                           
                                            <div class="form-group row">
                                                <label class="col-form-label col-sm-2">License Number <font color="red">*</font></label>
                                                <div class="col-sm-5">
                                                    <input type="text" name="license_no" id="license_no" class="form-control" required>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <label class="col-form-label col-sm-2">Province / State <font color="red">*</font></label>
                                                <div class="col-sm-5">
                                                    <select class="form-control" id="license_province" name="license_province" required>
                                                          <option  value="">Select</option> 
                                                           <?php

                                                            $this->db->select('*');
                                                            $this->db->from('web_mast_state_master');
                                                            $query = $this->db->get();
                                                            $data = $query->result_array();
                                                            foreach ($data as $rows) 
                                                            {
                                                                echo "<option value='" . $rows['id'] . "'>" . $rows['state'] . "</option>";
                                                            }
                                                        ?> 
                                                        <div id="state"></div>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <label class="col-form-label col-sm-2"> CDN Cycle <font color="red">*</font></label>
                                                <div class="col-sm-5">
                                                    <select class="form-control" id="cdn_cycle" name="cdn_cycle" required>
                                                          <option  value="">Select</option> 
                                                           <?php
                                                            $this->db->select('*');
                                                            $this->db->from('web_mast_cdn_cycle');
                                                            $query = $this->db->get();
                                                            $data = $query->result_array();
                                                            foreach ($data as $rows) 
                                                            {
                                                                echo "<option value='" . $rows['cycletype'] . "'>" . $rows['cycletype'] . "</option>";
                                                            }
                                                        ?> 
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-form-label col-sm-2"> US Cycle <font color="red">*</font></label>
                                                <div class="col-sm-5">
                                                    <select class="form-control" id="us_cycle" name="us_cycle" required>
                                                          <option  value="">Select</option> 
                                                           <?php
                                                        $this->db->select('*');
                                                            $this->db->from('web_mast_us_cycle');
                                                            $query = $this->db->get();
                                                            $data = $query->result_array();
                                                            foreach ($data as $rows) 
                                                            {
                                                                echo "<option value='" . $rows['cycletype'] . "'>" . $rows['cycletype'] . "</option>";
                                                            }
                                                        ?> 
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-form-label col-sm-2">License Issue Date <font color="red">*</font></label>
                                                <div class="col-sm-5">
                                                    <input type="date" name="license_issue_date" id="license_issue_date" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-form-label col-sm-2">License Expiry Date <font color="red">*</font></label>
                                                <div class="col-sm-5">
                                                    <input type="date" name="license_expiry_date" id="license_expiry_date" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-form-label col-sm-2">Medical Expiry Date <font color="red">*</font></label>
                                                <div class="col-sm-5">
                                                    <input type="date" name="medical_expiry_date" id="medical_expiry_date" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-form-label col-sm-2">Hire Date</label>
                                                <div class="col-sm-5">
                                                    <input type="date" name="hire_date" id="hire_date" class="form-control" >
                                                </div>
                                            </div>
                      <div class="form-group row">
                                                <label class="col-form-label col-sm-2">24-Hour Period Starting Time <font color="red">*</font></label>
                                                <div class="col-sm-5">
                                                    <input type="time" name="hr_start_period" id="hr_start_period" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-form-label col-sm-2">Status <font color="red">*</font></label>
                                                    <div class="col-sm-5">
                                                        <select class="form-control" id="state" name="state"  onchange="valueChanged()" required>
                                                            <option value="">Select</option>
                                                            <?php if($pst_state == "active"){ ?>
                                                            <option value="active" selected>Active (A)</option>
                                                            <option value="suspended">Suspended (S)</option>
                                                            <option value="terminated">Terminated (T)</option>
                                                            <option value="activer">Active-Rehired (AR)</option>
                                                            <?php } else if($pst_Status == "suspended") { ?>
                                                            <option value="active">Active (A)</option>
                                                            <option value="suspended" selected>Suspended (S)</option>
                                                            <option value="terminated">Terminated (T)</option>
                                                            <option value="activer">Active-Rehired (AR)</option>
                                                            <?php } else if($pst_Status == "terminated") { ?>
                                                            <option value="active">Active (A)</option>
                                                            <option value="suspended">Suspended (S)</option>
                                                            <option value="terminated" selected>Terminated (T)</option>
                                                            <option value="activer">Active-Rehired (AR)</option>
                                                            
                                                            <?php } else if($pst_Status == "activer") { ?>
                                                            <option value="active">Active (A)</option>
                                                            <option value="suspended">Suspended (S)</option>
                                                            <option value="terminated" >Terminated (T)</option>
                                                            <option value="activer"selected>Active-Rehired (AR)</option>
                                                            <?php } else { ?>
                                                            <option value="active">Active (A)</option>
                                                            <option value="suspended">Suspended (S)</option>
                                                            <option value="terminated">Terminated (T)</option>
                                                            <option value="activer">Active-Rehired (AR)</option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                            </div>
                                            <div id="terminal1">
                                            <div class="form-group row">
                                                <label class="col-form-label col-sm-2">Termination Date</label>
                                                <div class="col-sm-5">
                                                    <input type="date" name="terminal" id="terminal" class="form-control" >
                                                </div>
                                            </div>
                                        </div>
                                            <div class="form-group row">
                                                <label class="col-form-label col-sm-2">Driver Type <font color="red">*</font> </label>
                                                    <div class="col-sm-5">
                                                        <select class="form-control" id="drivertype" name="drivertype" required>
                                                            <option value="">Select</option>
                                                            <?php if($pst_drivertype == "citydriver"){ ?>
                                                            <option value="citydriver" selected>City Driver</option>
                                                            <option value="canadian_driver">Canadian Driver</option>
                                                            <option value="us_driver">US Driver</option>
                                                            <option value="eld_exempt">ELD Exempt</option>
                                                            <?php } else if($pst_drivertype == "canadian_driver") { ?>
                                                            <option value="citydriver">City Driver</option>
                                                            <option value="canadian_driver" selected>Canadian Driver</option>
                                                            <option value="us_driver">US Driver</option>
                                                            <option value="eld_exempt">ELD Exempt</option>
                                                            <?php } else if($pst_drivertype == "us_driver") { ?>
                                                            <option value="citydriver">City Driver</option>
                                                            <option value="canadian_driver">Canadian Driver</option>
                                                            <option value="us_driver" selected>US Driver</option>
                                                            <option value="eld_exempt">ELD Exempt</option>
                                                            <?php } else if($pst_drivertype == "eld_exempt") { ?>
                                                            <option value="citydriver">City Driver</option>
                                                            <option value="canadian_driver">Canadian Driver</option>
                                                            <option value="us_driver">US Driver</option>
                                                            <option value="eld_exempt" selected>ELD Exempt</option>
                                                            <?php }else { ?>
                                                            <option value="citydriver">City Driver</option>
                                                            <option value="canadian_driver">Canadian Driver</option>
                                                            <option value="us_driver">US Driver</option>
                                                            <option value="eld_exempt">ELD Exempt</option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                            </div>
                                            
                                            
                                            <div class="form-group row">
                                                <label class="col-form-label col-sm-2">Contractor </label>
                                                    <div class="col-sm-5">
                                                    <select class="form-control" id="contractor" name="contractor" >
                                                        <option value="">Select</option>
                                                        <?php if($pst_contractor == "Company Driver"){ ?>
                                                        <option value="Company Driver" selected>Company Driver</option>
                                                        <option value="Owner Operator">Owner Operator</option>
                                                        <?php } else if($pst_contractor == "Owner Operator") { ?>
                                                        <option value="Company Driver">Company Driver</option>
                                                        <option value="Owner Operator" selected>Owner Operator</option>
                                                        <?php } else { ?>
                                                        <option value="Company Driver">Company Driver</option>
                                                        <option value="Owner Operator">Owner Operator</option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-form-label col-sm-2">Hazmat Certified  </label>
                                                    <div class="col-sm-5">
                                                    <select class="form-control" id="hazmat_certified" name="hazmat_certified" onchange="hidehaz()">
                                                        <option  value="">Select</option>
                                                        <?php if($pst_hazmat_certified == "yes"){ ?>
                                                        <option value="yes" selected>YES</option>
                                                        <option value="no">NO</option>
                                                        <?php } else if($pst_hazmat_certified == "no") { ?>
                                                        <option value="yes">YES</option>
                                                        <option value="no" selected>NO</option>
                                                        <?php } else { ?>
                                                        <option value="yes">YES</option>
                                                        <option value="no">NO</option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            <div id="ht">
                                             <div class="form-group row">
                                                <label class="col-form-label col-sm-2"> Date of Issue</label>
                                                <div class="col-sm-5">
                                                    <input type="date" name="doi" id="doi" class="form-control" >
                                                </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <label class="col-form-label col-sm-2">Date of Expiry</label>
                                                <div class="col-sm-5">
                                                    <input type="date" name="doe" id="doe" class="form-control" >
                                                </div>
                                            </div>
                                        </div> 
                                            
                                            <div class="form-group row">
                                                <label class="col-form-label col-sm-2">FAST </label>
                                                    <div class="col-sm-5">
                                                    <select class="form-control" id="fast" name="fast"  onchange="fast1()">
                                                        <option  value="">Select</option>
                                                        <?php if($pst_fast == "yes"){ ?>
                                                        <option value="yes" selected>YES</option>
                                                        <option value="no">NO</option>
                                                        <?php } else if($pst_fast == "no") { ?>
                                                        <option value="yes">YES</option>
                                                        <option value="no" selected>NO</option>
                                                        <?php } else { ?>
                                                        <option value="yes">YES</option>
                                                        <option value="no">NO</option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>  
                                            
                                            <div id="ft">
                                                
                                           <div class="form-group row">
                                                <label class="col-form-label col-sm-2"> Date of Issue</label>
                                                <div class="col-sm-5">
                                                    <input type="date" name="f_doi" id="f_doi" class="form-control" >
                                                </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <label class="col-form-label col-sm-2">Date of Expiry</label>
                                                <div class="col-sm-5">
                                                    <input type="date" name="f_doe" id="f_doe" class="form-control" >
                                                </div>
                                            </div> 
                                            
                                            </div>
                                                
                                                
                                                
                                                
                                          
                                                <div class="form-group row">
                                                <label class="col-form-label col-sm-2">LCV</label>
                                                <div class="col-sm-5">
                                                    <input type="text" name="lcv" id="lcv" class="form-control" >
                                                </div>
                                            </div>
                                        
                                            <div class="form-group row">
                                                <label class="col-form-label col-sm-2">Home terminal(S) <font color="red">*</font></label>
                                                <div class="col-sm-5">
                                                    <select class="form-control" id="home_terminal" name="home_terminal" required>
                                                         <option  value="">Select</option>
                                                        <?php
                                                        $query = $this->db->query("SELECT cd.id,cd.add_line1,cd.city,co.name,st.state,cd.postalcode FROM web_map_company_add AS cd JOIN web_mast_country AS co ON co.id=cd.country JOIN web_mast_state_master AS st ON st.id=cd.state");
                                                        $resc = $query->result_array();
                                                        foreach ($resc as $rowc)
                                                            {
                                                                echo "<option value='" . $rowc['id'] . "'>" . $rowc['add_line1'].", ".$rowc['city'].", ".$rowc['name'].", ".$rowc['state'].", ".$rowc['postalcode']."</option>";
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                             <div class="form-group row">
                                                <label class="col-form-label col-sm-2">&nbsp;</label>
                                                <div class="col-sm-5">
                                                    <button type="submit" name="submit" class="btn btn-success">Submit</button>
                                                </div>
                                            </div>                 
                                        </form>
                                      </div>


              </div>
              <div class="col-md-2">
               </div>
            </div>
          </div>
        </section>
      </div>
      <?php include "footer.php" ?>

      <script>

                $(document).ready(function(){


                 /*datwpicker*/
              jQuery('.mydatepicker').datepicker({
              format: 'dd/mm/yyyy',
         });
        jQuery('#datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true
        });
            $('#create_driver').parsley();
        $( "#create_driver" ).submit(function( event ) {
          event.preventDefault();
          // var datas = $( '#create_job' ).serialize();
            var datas = new FormData(this);
          $.ajax({
                url: "<?php echo base_url()?>company/create_driver",
                type: "POST",
                dataType: "json",
                data: $("#create_driver").serialize(),
                    success:function(data){
                console.log(data);
              if(data.status) {
                
                 Swal.fire(data.status_text, '', 'success');
                    setTimeout(function(){ location.reload(); }, 2000);

                  }else{
                   Swal.fire('Failed to delete role details', '', 'warning');
                    return false;
                  }      
            }
          });
        });
        });


      </script>

    </div>
  </body>
</html>