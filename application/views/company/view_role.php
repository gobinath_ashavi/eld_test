<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo get_project_name_for_browser_tab(); ?> | View Roles</title>
    <?php include "header.php" ?>
  </head>
  <body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
      <?php include "sidebar.php" ?>
      <style type="text/css">
        .dataTables_filter {
            display: block;
        }
      </style>

      <div class="content-wrapper">
        <div class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1 class="m-0">View Roles</h1>
              </div>
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url()?>company/home">Dashboard</a></li>
                  <li class="breadcrumb-item active">View Roles</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <section class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-lg-12 col-12">

                <h3 style="text-align: center;">View Roles</h3>
                <div class="table-responsive">
                  <a href="<?php echo base_url()?>company/addRole" class="btn btn-info">Add New Role</a>
                  <table id="rolesList" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Role Name</th>
                        <th class="no-sort" style="width:12%;">Action</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                        <th>#</th>
                        <th>Role Name</th>
                        <th class="no-sort" style="width:12%;">Action</th>
                      </tr>
                    </tfoot>
                  </table>
                </div>

              </div>
            </div>
          </div>
        </section>
      </div>
      <?php include "footer.php" ?>

      <script>
        $(document).ready(function() {
          loadUsers();          
        });

        function loadUsers() {
          $('#rolesList').dataTable().fnClearTable();
          $('#rolesList').dataTable().fnDestroy();

          $('#rolesList')
          .on( 'order.dt',  function () { console.log('ORDER'); } )
          .on( 'search.dt', function () { console.log('SEARCH'); } )
          .on( 'page.dt',   function () { console.log('PAGE'); } )
          .DataTable({
            dom: 'Bfrtip',
            buttons: [
              'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            "bSort" : false,

            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": true,
            "responsive": false,

            // Load data for the table's content from an Ajax source
            "ajax": {
              //"url": "<?php //echo base_url(); ?>company/role/list",
              "url": "<?php echo base_url(); ?>company/getRoleList",
              "type": "POST",
              "data" : {}
            },
            //Set column definition initialisation properties.
            "columnDefs": [
              {
                "targets": [ 0 ], //first column / numbering column
                "orderable": false, //set not orderable
              },
            ],
          });
        }

        $(document).on('click', '#rolesList tbody td a.update_role_status', function(e) {
          e.stopImmediatePropagation();
          
          var role_id = $(this).data('role_id');
          var role_status = $(this).data('role_status');
          Swal.fire({
            title: '<div>Confirmation</div><br>',
            text: "Are you sure to " + (role_status == '1' ? 'delete' : 'active') + " role details ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false 
          }).then((result) => {
            if (result.value) {
              $.ajax({
                //url: "<?php //echo base_url()?>company/role/delete",
                url: "<?php echo base_url()?>company/deleteRole",
                type: "POST",
                dataType: "json",
                data: {
                  "id" : role_id,
                  "status" : role_status
                },
                success: function(data) {
                  if(data.status) {
                    Swal.fire(data.status_text, '', 'success');
                    setTimeout(function(){ location.reload(); }, 2000);
                  } else {
                    Swal.fire('Failed to delete role details', '', 'warning');
                    return false;
                  }
                }
              });
            }
          });
        });
      </script>

    </div>
  </body>
</html>