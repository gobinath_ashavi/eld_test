<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo get_project_name_for_browser_tab(); ?> | View Trailer</title>
    <?php include "header.php" ?>
  </head>
  <body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
      <?php include "sidebar.php" ?>
      <style type="text/css">
        .dataTables_filter {
            display: block;
        }
      </style>

      <div class="content-wrapper">
        <div class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1 class="m-0">View Trailer</h1>
              </div>
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url()?>company/home">Dashboard</a></li>
                  <li class="breadcrumb-item active">View Trailer</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <section class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-lg-12 col-12">

                <h3 style="text-align: center;">View Trailer</h3>
                <div class="table-responsive">
                  <div class="form-group row">
                    <div class="col-lg-3">
                      <select class="form-control" id="trailer_status" name="trailer_status">
                        <option value=""> -- Select Trailer Status -- </option>
                        <option value="1">Assigned</option>
                        <option value="0">Not Assigned</option>
                      </select>
                    </div>
                  </div>

                  <table id="trailerList" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Vin No</th>
                        <th>Make/Model</th>
                        <th>License Plate No</th>
                        <th>Unit No</th>
                        <th>Plate Expiry</th>
                        <th>Device ID</th>
                        <th>Fuel Capacity</th>
                        <th>Liters/Gallons</th>
                        <th class="no-sort" style="width:12%;">Action</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                        <th>#</th>
                        <th>Vin No</th>
                        <th>Make/Model</th>
                        <th>License Plate No</th>
                        <th>Unit No</th>
                        <th>Plate Expiry</th>
                        <th>Device ID</th>
                        <th>Fuel Capacity</th>
                        <th>Liters/Gallons</th>
                        <th class="no-sort" style="width:12%;">Action</th>
                      </tr>
                    </tfoot>
                  </table>
                </div>

              </div>
            </div>
          </div>
        </section>
      </div>
      <?php include "footer.php" ?>

      <script>
        $(document).ready(function() {
          loadTrailers();          
        });

        $(document).on('change', '#trailer_status', function(e) {
          e.stopImmediatePropagation();

          loadTrailers();
        });

        function loadTrailers() {
          $('#trailerList').dataTable().fnClearTable();
          $('#trailerList').dataTable().fnDestroy();

          $('#trailerList')
          .on( 'order.dt',  function () { console.log('ORDER'); } )
          .on( 'search.dt', function () { console.log('SEARCH'); } )
          .on( 'page.dt',   function () { console.log('PAGE'); } )
          .DataTable({
            dom: 'Bfrtip',
            buttons: [
              'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            "bSort" : false,

            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": true,
            "responsive": false,

            // Load data for the table's content from an Ajax source
            "ajax": {
              //"url": "<?php echo base_url(); ?>company/trailer/list",
              "url": "<?php echo base_url(); ?>company/getTrailerList",
              "type": "POST",
              "data" : {
                'trailer_status' : $("#trailer_status").val()
              }
            },
            //Set column definition initialisation properties.
            "columnDefs": [
              {
                "targets": [ 0 ], //first column / numbering column
                "orderable": false, //set not orderable
              },
            ],
          });
        }

        $(document).on('click', '#trailerList tbody td a.update_trailer_status', function(e) {
          e.stopImmediatePropagation();
          
          var trailer_id = $(this).data('trailer_id');
          var trailer_status = $(this).data('trailer_status');
          Swal.fire({
            title: '<div>Confirmation</div><br>',
            text: "Are you sure to " + (trailer_status == '1' ? 'delete' : 'active') + " trailer details ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false 
          }).then((result) => {
            if (result.value) {
              $.ajax({
                //url: "<?php //echo base_url()?>company/trailer/delete",
                url: "<?php echo base_url()?>company/deleteTrailer",
                type: "POST",
                dataType: "json",
                data: {
                  "id" : trailer_id,
                  "status" : trailer_status
                },
                success: function(data) {
                  if(data.status) {
                    Swal.fire(data.status_text, '', 'success');
                    setTimeout(function(){ location.reload(); }, 2000);
                  } else {
                    Swal.fire('Failed to delete trailer details', '', 'warning');
                    return false;
                  }
                }
              });
            }
          });
        });
      </script>

    </div>
  </body>
</html>