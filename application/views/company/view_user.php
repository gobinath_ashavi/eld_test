<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo get_project_name_for_browser_tab(); ?> | View Users</title>
    <?php include "header.php" ?>
  </head>
  <body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
      <?php include "sidebar.php" ?>
      <style type="text/css">
        .dataTables_filter {
            display: block;
        }
      </style>

      <div class="content-wrapper">
        <div class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1 class="m-0">View Users</h1>
              </div>
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url()?>company/home">Dashboard</a></li>
                  <li class="breadcrumb-item active">View Users</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <section class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-lg-12 col-12">

                <h3 style="text-align: center;">View Users</h3>
                <div class="table-responsive">
                  <table id="usersList" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>User ID</th>
                        <th>User Name</th>
                        <th>User Type</th>
                        <th>Full Name</th>
                        <th>Mobile</th>
                        <th>Work Phone</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th class="no-sort" style="width:12%;">Action</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                        <th>#</th>
                        <th>User ID</th>
                        <th>User Name</th>
                        <th>User Type</th>
                        <th>Full Name</th>
                        <th>Mobile</th>
                        <th>Work Phone</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th class="no-sort" style="width:12%;">Action</th>
                      </tr>
                    </tfoot>
                  </table>
                </div>

              </div>
            </div>
          </div>
        </section>
      </div>
      <?php include "footer.php" ?>

      <script>
        $(document).ready(function() {
          loadUsers();          
        });

        function loadUsers() {
          $('#usersList').dataTable().fnClearTable();
          $('#usersList').dataTable().fnDestroy();

          $('#usersList')
          .on( 'order.dt',  function () { console.log('ORDER'); } )
          .on( 'search.dt', function () { console.log('SEARCH'); } )
          .on( 'page.dt',   function () { console.log('PAGE'); } )
          .DataTable({
            dom: 'Bfrtip',
            buttons: [
              'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            "bSort" : false,

            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": true,
            "responsive": false,

            // Load data for the table's content from an Ajax source
            "ajax": {
              //"url": "<?php echo base_url(); ?>company/user/list",
              "url": "<?php echo base_url(); ?>company/getUserList",
              "type": "POST",
              "data" : {}
            },
            //Set column definition initialisation properties.
            "columnDefs": [
              {
                "targets": [ 0 ], //first column / numbering column
                "orderable": false, //set not orderable
              },
            ],
          });
        }

        $(document).on('click', '#usersList tbody td a.update_user_status', function(e) {
          e.stopImmediatePropagation();
          
          var user_id = $(this).data('user_id');
          var user_status = $(this).data('user_status');
          Swal.fire({
            title: '<div>Confirmation</div><br>',
            text: "Are you sure to " + (user_status == '1' ? 'delete' : 'active') + " user details ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false 
          }).then((result) => {
            if (result.value) {
              $.ajax({
                //url: "<?php //echo base_url()?>company/driver/delete",
                url: "<?php echo base_url()?>company/deleteUser",
                type: "POST",
                dataType: "json",
                data: {
                  "id" : user_id,
                  "status" : user_status
                },
                success: function(data) {
                  if(data.status) {
                    Swal.fire(data.status_text, '', 'success');
                    setTimeout(function(){ location.reload(); }, 2000);
                  } else {
                    Swal.fire('Failed to delete user details', '', 'warning');
                    return false;
                  }
                }
              });
            }
          });
        });

        $(document).on('click', '#usersList tbody td a.switch_user', function(e) {
          e.stopImmediatePropagation();
          
          var user_id = $(this).data('user_id');
          var company_id = $(this).data('company_id');
          var user_type = $(this).data('user_type');

          Swal.fire({
            title: '<div>Confirmation</div><br>',
            text: "Are you sure switch to user details ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false 
          }).then((result) => {
            if (result.value) {
              $.ajax({
                //url: "<?php //echo base_url()?>company/driver/delete",
                url: "<?php echo base_url()?>company/switchUser",
                type: "POST",
                dataType: "json",
                data: {
                  "id" : user_id,
                  "company_id" : company_id,
                  "user_type" : user_type
                },
                success: function(data) {
                  if(data.status)
                    setTimeout(function(){ location.href = data.url; }, 1000);
                  else {
                    Swal.fire('Failed to switch user details', '', 'warning');
                    return false;
                  }
                }
              });
            }
          });
        });
      </script>

    </div>
  </body>
</html>