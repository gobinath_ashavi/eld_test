<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo get_project_name_for_browser_tab(); ?> | Edit Role</title>
    <?php include "header.php" ?>
  </head>
  <body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
      <?php include "sidebar.php" ?>
      <style type="text/css">
        .dataTables_filter {
            display: block;
        }
      </style>

      <div class="content-wrapper">
        <div class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1 class="m-0">Edit Role</h1>
              </div>
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url()?>company/home">Dashboard</a></li>
                  <li class="breadcrumb-item"><a href="<?php echo base_url()?>company/viewRole">View Roles</a></li>
                  <li class="breadcrumb-item active">Edit Role</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <section class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-lg-12 col-12">
                <h3 style="text-align: center;">Edit Role</h3>
                
                <form class="form-horizontal form-material" id="update_role_form" method="POST" action="">
                  <br>
                  <input type="hidden" name="role_id" id="role_id" value="<?php echo $role_id; ?>">
                  <div class="form-group row">
                    <label class="col-form-label col-sm-2">Role Name <font color="red">*</font></label>
                    <div class="col-sm-4">
                      <input type="text" name="role_name" class="form-control" id="role_name" value="<?php echo $role_detail['name']; ?>" placeholder="Enter Role Name">
                    </div>
                  </div>
                  <br>
                  <div class="form-group row">
                    <label class="col-form-label col-sm-2">Module Access <font color="red">*</font></label>
                    <div class="col-sm-4">
                      <input type="checkbox" id="checkAll" /> Select All
                      <?php
                        $module = $this->common->getRawQueryResult("SELECT * FROM web_mast_modules");
                        if(count($module) > 0) {
                          foreach ($module as $k => $v) {
                            $checked = "";
                            if(in_array($v['id'], explode(",",$role_detail['modules'])))
                              $checked = "checked='checked'";
                            echo "<br /> <input type='checkbox' class='checkItem' name='chk[]' value='" . $v['id'] . "'" . $checked . " ' id='chkLicence'/> " . $v['name'];
                          
                            $sub_module = $this->common->getRawQueryResult("SELECT id, sname FROM web_map_sub_modules WHERE mid = " . $v['id']);
                            if(count($sub_module) > 0) {
                              foreach ($sub_module as $sk => $sv) {
                                $sk_checked = "";
                                if(in_array($sv['id'], explode(",",$role_detail['sub_modules'])))
                                  $sk_checked = "checked='checked'";
                                echo "<br /> <input type='checkbox' class='checkItem' name='sub_chk[]' value='" . $sv['id'] . "'" . $sk_checked . "' style='margin-left:50px;'/> " . $sv['sname'];
                              }
                            }
                          }
                        }
                      ?>
                    </div>
                  </div>
                  <br>
                  <div class="form-group row">
                    <label class="col-form-label col-sm-2">&nbsp;</label>
                    <div class="col-sm-4">
                      <button type="button" name="submit" id="update_role" class="btn btn-sm btn-primary">Submit</button>
                    </div>
                  </div>  
                </form>
              </div>
            </div>
          </div>
        </section>
      </div>
      <?php include "footer.php" ?>

      <script>
        $(document).on('click', '#checkAll', function(e) {
          e.stopImmediatePropagation();

          $('input:checkbox').not(this).prop('checked', this.checked);
        });

        $(document).on('click', '#update_role', function(e) {
          e.stopImmediatePropagation();
          
          var role_id = $("#role_id").val();
          var role_name = $("#role_name").val();
          var chkCnt = $(".checkItem:checked").length;

          if(role_id == 0) {
            Swal.fire('Role id should not be zero', '', 'warning');
            return false;
          }

          if(role_name == "" || role_name == "undefined" || role_name == null) {
            if($("#role_name").val().trim() == "" || $("#role_name").val().trim() == "undefined" || $("#role_name").val().trim() == null || $("#role_name").val().trim().length == 0) {
              Swal.fire('Role name should not be empty', '', 'warning');
              return false;
            }
          }

          if(chkCnt == 0) {
            Swal.fire('Select access permission', '', 'warning');
            return false;
          }

          Swal.fire({
            title: '<div>Confirmation</div><br>',
            text: "Are you sure to update role details ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false 
          }).then((result) => {
            if (result.value) {
              $.ajax({
                //url: "<?php //echo base_url()?>company/role/delete",
                url: "<?php echo base_url()?>company/updateRole",
                type: "POST",
                dataType: "json",
                data: $("#update_role_form").serialize(),
                success: function(data) {
                  if(data.status) {
                    Swal.fire(data.status_text, '', 'success');
                    setTimeout(function(){ location.reload(); }, 2000);
                  } else {
                    Swal.fire('Failed to delete role details', '', 'warning');
                    return false;
                  }
                }
              });
            }
          });
        });
      </script>

    </div>
  </body>
</html>