<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo get_project_name_for_browser_tab(); ?> | Add Role</title>
    <?php include "header.php" ?>
  </head>
  <body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
      <?php include "sidebar.php" ?>
      <style type="text/css">
        .dataTables_filter {
            display: block;
        }
      </style>

      <div class="content-wrapper">
        <div class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1 class="m-0">Add Role</h1>
              </div>
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url()?>company/home">Dashboard</a></li>
                  <li class="breadcrumb-item"><a href="<?php echo base_url()?>company/viewRole">View Roles</a></li>
                  <li class="breadcrumb-item active">Add Role</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <section class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-lg-12 col-12">
                <h3 style="text-align: center;">Add Role</h3>
                
                <form class="form-horizontal form-material" id="add_role_form" method="POST" action="">
                  <br>
                  <div class="form-group row">
                    <label class="col-form-label col-sm-2">Role Name <font color="red">*</font></label>
                    <div class="col-sm-4">
                      <input type="text" name="role_name" class="form-control" id="role_name" value="" placeholder="Enter Role Name">
                    </div>
                  </div>
                  <br>
                  <div class="form-group row">
                    <label class="col-form-label col-sm-2">Module Access <font color="red">*</font></label>
                    <div class="col-sm-4">
                      <input type="checkbox" id="checkAll" /> Select All
                      <?php
                        $module = $this->common->getRawQueryResult("SELECT * FROM web_mast_modules");
                        if(count($module) > 0) {
                          foreach ($module as $k => $v) {
                            //$checked = "checked='checked'";
                            $checked = "";
                            echo "<br /> <input type='checkbox' class='checkItem' name='chk[]' value='" . $v['id'] . "'" . $checked . " ' id='chkLicence'/> " . $v['name'];
                          
                            $sub_module = $this->common->getRawQueryResult("SELECT id, sname FROM web_map_sub_modules WHERE mid = " . $v['id']);
                            if(count($sub_module) > 0) {
                              foreach ($sub_module as $sk => $sv) {
                                //$sk_checked = "checked='checked'";
                                $sk_checked = "";
                                echo "<br /> <input type='checkbox' class='checkItem' name='sub_chk[]' value='" . $sv['id'] . "'" . $sk_checked . "' style='margin-left:50px;'/> " . $sv['sname'];
                              }
                            }
                          }
                        }
                      ?>
                    </div>
                  </div>
                  <br>
                  <div class="form-group row">
                    <label class="col-form-label col-sm-2">&nbsp;</label>
                    <div class="col-sm-4">
                      <button type="button" name="submit" id="save_role" class="btn btn-sm btn-primary">Submit</button>
                    </div>
                  </div>  
                </form>
              </div>
            </div>
          </div>
        </section>
      </div>
      <?php include "footer.php" ?>

      <script>
        $(document).on('click', '#checkAll', function(e) {
          e.stopImmediatePropagation();

          $('input:checkbox').not(this).prop('checked', this.checked);
        });

        $(document).on('click', '#save_role', function(e) {
          e.stopImmediatePropagation();
          
          var role_name = $("#role_name").val();
          var chkCnt = $(".checkItem:checked").length;

          if(role_name == "" || role_name == "undefined" || role_name == null) {
            if($("#role_name").val().trim() == "" || $("#role_name").val().trim() == "undefined" || $("#role_name").val().trim() == null || $("#role_name").val().trim().length == 0) {
              Swal.fire('Role name should not be empty', '', 'warning');
              return false;
            }
          }

          if(chkCnt == 0) {
            Swal.fire('Select access permission', '', 'warning');
            return false;
          }

          Swal.fire({
            title: '<div>Confirmation</div><br>',
            text: "Are you sure to add role details ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false 
          }).then((result) => {
            if (result.value) {
              $.ajax({
                //url: "<?php //echo base_url()?>company/role/delete",
                url: "<?php echo base_url()?>company/saveRole",
                type: "POST",
                dataType: "json",
                data: $("#add_role_form").serialize(),
                success: function(data) {
                  if(data.status) {
                    Swal.fire(data.status_text, '', 'success');
                    setTimeout(function(){ location.reload(); }, 2000);
                  } else {
                    Swal.fire('Failed to delete role details', '', 'warning');
                    return false;
                  }
                }
              });
            }
          });
        });
      </script>

    </div>
  </body>
</html>