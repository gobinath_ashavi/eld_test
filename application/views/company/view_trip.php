<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo get_project_name_for_browser_tab(); ?> | View Trip</title>
    <?php include "header.php" ?>
  </head>
  <body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
      <?php include "sidebar.php" ?>
      <style type="text/css">
        .dataTables_filter {
            display: block;
        }
      </style>

      <div class="content-wrapper">
        <div class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1 class="m-0">View Trip</h1>
              </div>
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url()?>company/home">Dashboard</a></li>
                  <li class="breadcrumb-item active">View Trip</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <section class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-lg-12 col-12">

                <h3 style="text-align: center;">View Trip</h3>
                <div class="table-responsive">
                  <div class="form-group row" style="display: none;">
                    <div class="col-lg-3">
                      <select class="form-control" id="trip_status" name="trip_status">
                        <option value=""> -- Select Trip Status -- </option>
                        <option value="1">Assigned</option>
                        <option value="0">Not Assigned</option>
                      </select>
                    </div>
                  </div>

                  <table id="tripList" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Trip Number</th>
                        <th>Name</th>
                        <th>Truck</th>
                        <th>Trailer</th>
                        <th>Start Point</th>
                        <th>End Point</th>
                        <th>Start Date</th>
                        <th>Expected End Date</th>
                        <th>Driver Name</th>
                        <th>Co Driver Name</th>
                        <th class="no-sort" style="width:12%;">Action</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                        <th>#</th>
                        <th>Trip Number</th>
                        <th>Name</th>
                        <th>Truck</th>
                        <th>Trailer</th>
                        <th>Start Point</th>
                        <th>End Point</th>
                        <th>Start Date</th>
                        <th>Expected End Date</th>
                        <th>Driver Name</th>
                        <th>Co Driver Name</th>
                        <th class="no-sort" style="width:12%;">Action</th>
                      </tr>
                    </tfoot>
                  </table>
                </div>

              </div>
            </div>
          </div>
        </section>
      </div>
      <?php include "footer.php" ?>



      <!-- modal content -->
      <div id="assign_driver_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="myModalLabel">Assign / Unassign Driver & Co-Driver</h4>
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
              <form class="form-horizontal form-material" id="assign_driver_form" method="post">
                <input type="hidden" id="trip_id" value="0">
                <input type="hidden" id="company_id" value="0">

                <div class="form-group row">
                  <label class="col-sm-4">Trip Number</label>
                  <div class="col-sm-8">
                    <span id="trip_number"></span>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-form-label col-sm-4">Assign / Unassign Driver </label>
                  <div class="col-sm-8">
                    <select class="form-control form-control-line" id="driver_id" name="driver_id">
                    </select>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-form-label col-sm-4">Assign / Unassign Co-Driver </label>
                  <div class="col-sm-8">
                    <select class="form-control form-control-line" id="co_driver_id" name="co_driver_id">
                    </select>
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button class="btn btn-info" type="button" id="assign_driver_info">Submit</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal" id="">Close</button>
            </div>
          </div>
        </div>
      </div>

      <script>
        $(document).ready(function() {
          loadTrips();          
        });

        $(document).on('change', '#trip_status', function(e) {
          e.stopImmediatePropagation();

          loadTrips();
        });

        function loadTrips() {
          $('#tripList').dataTable().fnClearTable();
          $('#tripList').dataTable().fnDestroy();

          $('#tripList')
          .on( 'order.dt',  function () { console.log('ORDER'); } )
          .on( 'search.dt', function () { console.log('SEARCH'); } )
          .on( 'page.dt',   function () { console.log('PAGE'); } )
          .DataTable({
            dom: 'Bfrtip',
            buttons: [
              'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            "bSort" : false,

            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": true,
            "responsive": false,

            // Load data for the table's content from an Ajax source
            "ajax": {
              //"url": "<?php echo base_url(); ?>company/trip/list",
              "url": "<?php echo base_url(); ?>company/getTripList",
              "type": "POST",
              "data" : {
                //'trip_status' : $("#trip_status").val()
              }
            },
            //Set column definition initialisation properties.
            "columnDefs": [
              {
                "targets": [ 0 ], //first column / numbering column
                "orderable": false, //set not orderable
              },
            ],
          });
        }

        $(document).on('click', '#tripList tbody td a.update_trip_status', function(e) {
          e.stopImmediatePropagation();
          
          var trip_id = $(this).data('trip_id');
          var trip_status = $(this).data('trip_status');
          Swal.fire({
            title: '<div>Confirmation</div><br>',
            text: "Are you sure to " + (trip_status == '1' ? 'delete' : 'active') + " trip details ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false 
          }).then((result) => {
            if (result.value) {
              $.ajax({
                //url: "<?php //echo base_url()?>company/trip/delete",
                url: "<?php echo base_url()?>company/deleteTrip",
                type: "POST",
                dataType: "json",
                data: {
                  "id" : trip_id,
                  "status" : trip_status
                },
                success: function(data) {
                  if(data.status) {
                    Swal.fire(data.status_text, '', 'success');
                    setTimeout(function(){ location.reload(); }, 2000);
                  } else {
                    Swal.fire('Failed to delete trip details', '', 'warning');
                    return false;
                  }
                }
              });
            }
          });
        });

        $(document).on('click', '#tripList tbody td a.assign_driver_codriver', function(e) {
          e.stopImmediatePropagation();
          
          var trip_id = $(this).data('trip_id');
          var company_id = $(this).data('company_id');
          var trip_number = $(this).data('trip_number');
          var assigned_driver_id = $(this).data('driver_id');
          var assigned_co_driver_id = $(this).data('co_driver_id');

          if(trip_id == "" || trip_id == "0" || trip_id == "undefined" || trip_id == null) {
            Swal.fire('Trip id not found', '', 'warning');
            return false;
          }

          $('#driver_id, #co_driver_id').html('');
          Swal.fire({
            title: '<div>Confirmation</div><br>',
            text: "Are you sure to assign / unassign driver & co-driver for the trip details ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false 
          }).then((result) => {
            if (result.value) {
              $.ajax({
                //url: "<?php //echo base_url()?>company/trip/delete",
                url: "<?php echo base_url()?>company/getUserForAssignDriverCoDriver",
                type: "POST",
                dataType: "json",
                data: {
                  "trip_id" : trip_id,
                  "company_id" : company_id
                },
                success: function(data) {
                  if(data.status) {
                    $('#assign_driver_modal #assign_driver_form #trip_id').val(trip_id);
                    $('#assign_driver_modal #assign_driver_form #company_id').val(company_id);
                    $('#assign_driver_modal #assign_driver_form #trip_number').html(trip_number);
                    $('#assign_driver_modal #assign_driver_form #driver_id').html(renderDriverOption('Driver', data.driver_users, assigned_driver_id));
                    $('#assign_driver_modal #assign_driver_form #co_driver_id').html(renderDriverOption('Co-Driver', data.co_driver_users, assigned_co_driver_id));
                    $('#assign_driver_modal').modal('show');
                  } else {
                    Swal.fire('Failed to get trip driver details', '', 'warning');
                    return false;
                  }
                }
              });
            }
          });
        });

        $(document).on('click', '#assign_driver_modal #assign_driver_info', function(e) {
          e.stopImmediatePropagation();

          var trip_id = $('#assign_driver_modal #assign_driver_form #trip_id').val();
          var company_id = $('#assign_driver_modal #assign_driver_form #company_id').val();
          var driver_id = $('#assign_driver_modal #assign_driver_form #driver_id').val();
          var co_driver_id = $('#assign_driver_modal #assign_driver_form #co_driver_id').val();

          if(trip_id == "" || trip_id == "0" || trip_id == "undefined" || trip_id == null) {
            Swal.fire('Trip id not found', '', 'warning');
            return false;
          }

          Swal.fire({
            title: '<div>Confirmation</div><br>',
            text: "Are you sure to save / update driver & co-driver for the trip details ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false 
          }).then((result) => {
            if (result.value) {
              $.ajax({
                //url: "<?php //echo base_url()?>company/trip/delete",
                url: "<?php echo base_url()?>company/getSaveUpdateDriverCoDriver",
                type: "POST",
                dataType: "json",
                data: {
                  "trip_id" : trip_id,
                  "company_id" : company_id,
                  "driver_id" : driver_id,
                  "co_driver_id" : co_driver_id
                },
                success: function(data) {
                  if(data.status) {
                    Swal.fire(data.status_text, '', 'success');
                    return false;
                  } else {
                    Swal.fire('Failed to save / update driver & co-driver for the trip details', '', 'warning');
                    return false;
                  }
                }
              });
            }
          });
        });

        function renderDriverOption(type, user_data, assigned_user) {
          var html = '<option value=""> -- Select ' + type + ' -- </option>';
          if(user_data.length > 0) {
            $(user_data).each(function(i, v) {
              var selected = '';
              if(assigned_user != "")
                selected = ((v.id == assigned_user) ? 'selected="selected"' : '');
              html += '<option value="' + v.id + '" ' + selected + '>' + v.name + '</option>';
            });
          }
          return html;
        }
      </script>

    </div>
  </body>
</html>