<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Ashaviglobal | Log in</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url()?>template/assets/plugins/fontawesome-free/css/all.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="<?php echo base_url()?>template/assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url()?>template/assets/dist/css/adminlte.min.css">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <img style="height: 100px;border-radius: 10px" src="<?php echo base_url()?>template/images/logo.jpg">
    <a href="<?php echo base_url()?>"><b>Ashavi</b>Global</a>

  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <h3 align="center">Login</h3>

      <form id="loginform">
        <div class="input-group mb-3">
          <input type="text" name="email" class="form-control" placeholder="Email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" name="password" class="form-control" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember">
              <label for="remember">
                Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block login_button">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
<!-- 
      <div class="social-auth-links text-center mb-3">
        
        <input type="radio" name="role" id="superadmin" value="superadmin">
        <label>Superadmin</label>

        
        <input type="radio" name="role" id="company" value="company">
         <label>Company</label>
        
        <input type="radio" name="role" id="user" value="user">
        <label>user</label>     

      </div> -->
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="<?php echo base_url()?>template/assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url()?>template/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url()?>template/assets/dist/js/adminlte.min.js"></script>






<script type="text/javascript">
         $(document).ready(function(){
          $("#loginform").submit(function( event ) {
           $(".login_button").html('Please wait');

           $(".login_button").prop("disabled", true);
            event.preventDefault();
            var datas = $('#loginform').serialize();


// console.log(datas);
// return false;

            $.ajax({

              url:"<?php echo base_url()?>auth/authenticate",

              method:'post',

              data:datas,

              success:function(data){
                var res = jQuery.parseJSON(data);

                // console.log(res.status);
                if (res.status == true) {
                    // alert(res.status_text);
                    $(".login_button").html('Login success');

                  //   $.toast({
                  //   heading: 'Success',
                  //   text: res.status_text,
                  //   position: 'top-right',
                  //   loaderBg:'#ff6849',
                  //   icon: 'success',
                  //   hideAfter: 3500, 
                  //   stack: 6
                  // });

                  setTimeout(function(){ location.reload(); }, 1000);
                //      setTimeout(function(){ 
                //   window.location = "<?php echo base_url()."auth/two_step_verification"; ?>";
                  
                // }, 1000);
                }else{
                    // alert('failed');
                    $(".login_button").html('Log In');

                    $(".login_button").prop("disabled", false);
                    $.toast({

                    heading: 'Failed',

                    text: res.status_text,

                    position: 'top-right',

                    loaderBg:'#ff6849',

                    icon: 'error',

                    hideAfter: 3500

                  });

                  // setTimeout(function(){ location.reload(); }, 1000);
                }    
              }
            });
          });

        });
    </script>
</body>
</html>
