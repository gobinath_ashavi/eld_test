<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 *	Routes Alias Name : company
 *	Session Name : company
 */
class Company extends CI_Controller
{
	public function __construct() {
		parent::__construct();
		$this->_app = $this->config->item('TABLE_APP_PREFIX');
		$this->_web = $this->config->item('TABLE_WEB_PREFIX');
        $this->_map = $this->config->item('TABLE_MAP_PREFIX');
        $this->_mast = $this->config->item('TABLE_MAST_PREFIX');
	}

	/**
	 *  Company Home / Dashboard
	 */
	public function home() {
		$this->session->unset_userdata('user');
		if ($this->session->userdata('company')) {
			if ($this->session->userdata('company')['role'] == 2) {
				// company
				$cond = array('company_id' => $this->session->userdata('company')['company_id'],'is_deleted' => 0);
				$data['truck_count'] = $this->common->fetchData('web_mast_truck', 'id', $cond, '', 1);

				$data['trailer_count'] = $this->common->fetchData('web_mast_trailer', 'id', $cond, '', 1);

				$data['driver_count'] = $this->common->fetchData('web_mast_driver', 'id', $cond, '', 1);

				$data['trip_count'] = $this->common->fetchData('web_mast_trip', 'id', $cond, '', 1);

				// $cond = array('company_id' => $this->session->userdata('company')['company_id'],'is_deleted' => 0);
				// $data['truck_count'] = $this->common->fetchData('web_mast_truck', 'id', $cond, '', 1);

				$this->load->view('company/home', $data);
			}
		} else
			redirect('auth/signin');
	}

	/********************************************/
	/**********************		DRIVER MODULE STARTS	 **********************/
	/********************************************/
	/**
	 *  Render Driver View Page
	 */
	public function viewDriver() {
		$this->load->view('company/view_driver', array('driver' => 'active'));
	}

	/**
	 *  Get All Driver List
	 */
	public function getDriverList() {
		$data['data'] = array();
		$data['status'] = 'empty';
		$data['recordsFiltered'] = 0;
		$data['recordsTotal'] = 0;
		$data['draw'] = $this->input->post('draw');

		$where = "";
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$driver_type = $this->input->post('driver_type');
		if(isset($driver_type) && !empty($driver_type))
			$where .= " AND wmd.driver_type = '" . $driver_type . "' ";

		$search = $this->input->post('search');
		if(isset($search['value']) && !empty($search['value']))
			$where .= " AND ( wmd.id = '" . $search['value'] . "' OR wmd.driver_id = '" . $search['value'] . "' OR wmd.name LIKE '%" . $search['value'] . "%' OR wmd.lastname LIKE '%" . $search['value'] . "%' OR wmd.phone LIKE '%" . $search['value'] . "%' OR wmd.mobile2 LIKE '%" . $search['value'] . "%' OR wmd.license_no LIKE '%" . $search['value'] . "%' OR wmd.license_province LIKE '%" . $search['value'] . "%' OR wmd.status LIKE '%" . $search['value'] . "%' OR sm.state LIKE '%" . $search['value'] . "%'	OR ua.email LIKE '%" . $search['value'] . "%' OR wmd.cdn_cycle LIKE '%" . $search['value'] . "%' OR wmd.us_cycle LIKE '%" . $search['value'] . "%' OR wmd.home_terminal LIKE '%" . $search['value'] . "%' OR cd.add_line1 LIKE '%" . $search['value'] . "%' OR cd.city LIKE '%" . $search['value'] . "%' OR co.name LIKE '%" . $search['value'] . "%' OR st.state LIKE '%" . $search['value'] . "%' OR cd.postalcode LIKE '%" . $search['value'] . "%' OR t.timezone LIKE '%" . $search['value'] . "%' OR t.fname LIKE '%" . $search['value'] . "%' OR log.city LIKE '%" . $search['value'] . "%' OR log.state LIKE '%" . $search['value'] . "%' ) ";

		if ($this->session->userdata('company')) {
			if ($this->session->userdata('company')['role'] == 2) {
				$company_id = $this->session->userdata('company')['company_id'];
				if(isset($company_id) && !empty($company_id) && $company_id != 0) {
					$sql = "SELECT wmd.id, wmd.driver_id, wmd.name, wmd.lastname, wmd.phone, wmd.mobile2, wmd.license_no, wmd.license_province, wmd.status, wmd.state, wmd.is_deleted, sm.state AS statename, ua.email, wmd.cdn_cycle, wmd.us_cycle, wmd.home_terminal, cd.add_line1 AS comp_addr, cd.city AS comp_city, co.name AS comp_name, st.state AS comp_state, cd.postalcode AS comp_zip, t.timezone, t.fname, log.city AS log_city, log.state AS log_state
						FROM " . $this->_web . $this->_mast . "driver AS wmd
						JOIN " . $this->_web . $this->_mast . "state_master AS sm ON sm.id = wmd.license_province
						JOIN " . $this->_web . $this->_mast . "auth_user AS ua ON ua.id = wmd.user_id
						LEFT JOIN " . $this->_web . $this->_map . "company_add AS cd ON cd.id = wmd.home_terminal AND cd.company_id = wmd.id
						LEFT JOIN " . $this->_web . $this->_mast . "country AS co ON co.id = cd.country
						LEFT JOIN " . $this->_web . $this->_mast . "state_master AS st ON st.id = cd.state
						LEFT JOIN " . $this->_web . $this->_mast . "company AS c ON c.id = wmd.id
						LEFT JOIN " . $this->_web . $this->_mast . "timezonelist AS t ON t.id = c.timezone
						LEFT JOIN " . $this->_app . $this->_mast . "change_duty_log AS LOG ON driverid = wmd.id
						WHERE wmd.company_id = '" . $company_id . "' " . $where . " GROUP BY wmd.id ";

					$getSqlCnt = $this->common->getRawQueryResult($sql);

					$sql .= " ORDER BY wmd.id DESC LIMIT " . $start . ", " . $length;

					$getSqlList = $this->common->getRawQueryResult($sql);
					
					if (count($getSqlList) > 0) {
						$data['draw'] = $this->input->post('draw');
						$data['recordsFiltered'] = count($getSqlCnt);
						$data['recordsTotal'] = count($getSqlCnt);
						$data['data'] = array();
						$no = $start;
						foreach ($getSqlList as $k) {
							$no++;
							$row = array();
							$row[] = $no;
							$row[] = $k['driver_id'];
                            $row[] = $k['name']." ".$k['lastname'];
                            $row[] = $k['license_no'];
                            $row[] = $k['license_province'];
                            $row[] = $k['cdn_cycle'];
                            $row[] = $k['us_cycle'];
                            $row[] = $k['phone'].",".$k['email'];
                            if(isset($k['comp_name']) && !empty($k['comp_name']))
							  	$row[] = $k['comp_addr'].", ".$k['comp_city'].", ".$k['comp_name'].", ".$k['comp_state'].", ".$k['comp_zip'];
							else
							    $row[] = "-";
                            if(isset($k['log_city']) && !empty($k['log_city']))
							  	$row[] = $k['log_city'].", ".$k['log_state'];
							else
							  	$row[] = "-";

                            $row[] = $k['fname'];

                            $status = array('active' => 'Active (A)', 'suspended' => 'Suspended (S)', 'terminated' => 'Terminated (T)', 'activer' => 'Active-Rehired (AR)');
							$row[] = (isset($k['state']) && !empty($k['state']) ? $status[$k['state']] : "-");
							//$row[] = ($k['is_deleted'] == '1' ? 'Deactive' : 'Active');

							$action = "";
							/*$action .= '<a href="portal_updatedriver.php?did=<?php echo $driver['id'];?>" data-toggle="tooltip" title="Update Driver"><i class="fa fa-edit" style="font-size:18px;color:#008000;"></i></a>';
							$action .= '<a href="driver_nlog.php?did=<?php echo $driver['id'];?>" data-toggle="tooltip" title="Driver Logs"><i class="fa fa-history" style="font-size:18px;color:#001000;"></i></a>';
                            if($k['status'] == 0)
								$action .= '<a onclick="deactivestatus(<?php echo $driver['id'];?>)" data-toggle="tooltip" title="Active Status" class="px-1"><i class="fa fa-eye" style="font-size:18px;color:#339933;"></i></a>';
                            else
                                $action .= '<a onclick="activestatus(<?php echo $driver['id'];?>)" data-toggle="tooltip" title="Deactive Status" class="px-1"><i class="fa fa-eye-slash" style="font-size:18px;color:#ff0000;"></i></a>';*/
                                $url = base_url().'company/edit_driver';
							$action .= '<a href="'.$url.'" data-toggle="tooltip" title="Update Driver"><i class="fa fa-edit" style="font-size:18px;color:#008000;"></i></a> <br>';
							$action .= '<a href="#" data-toggle="tooltip" title="Driver Logs"><i class="fa fa-history" style="font-size:18px;color:#001000;"></i></a> <br>';
                            if($k['is_deleted'] == '1')
								$action .= '<a data-toggle="tooltip" title="Activate Driver" class="px-1 update_driver_status" data-driver_status="0" data-driver_id="'.$k['id'].'"><i class="fa fa-eye" style="font-size:18px; color:#339933;"></i></a>';
                            else
                                $action .= '<a data-toggle="tooltip" title="Delete Driver" class="px-1 update_driver_status" data-driver_status="1" data-driver_id="'.$k['id'].'"><i class="fa fa-eye-slash" style="font-size:18px; color:#ff0000;"></i></a>';

                            $row[] = $action;
							
							$data['data'][] = $row;
						}
						$data['status'] = 'success';
					} else
						$data['status'] = 'empty';
				} else
					$data['status'] = 'Company info not found';
			} else 
				$data['status'] = 'User role not matched to this class';
		} else 
			$data['status'] = 'Session not found';

		echo json_encode($data);
	}

	/**
	 *  Delete Driver
	 */
	public function deleteDriver()
	{
		$data = array('is_deleted' => $this->input->post('status'));
		$cond = array('id' => $this->input->post('id'));
		$table_name = $this->_web.$this->_mast.'driver';
		$this->common->update($data, $cond, $table_name);
		if($this->input->post('status') == '0')
			$json_data = array('status' => true, 'status_text' => 'Driver details activated');
		else
			$json_data = array('status' => true, 'status_text' => 'Driver details deleted');

		echo json_encode($json_data);
	}
	/********************************************/
	/**********************		DRIVER MODULE ENDS	 **********************/
	/********************************************/




	/********************************************/
	/**********************		USER MODULE STARTS	 **********************/
	/********************************************/
	/**
	 *  Render User View Page
	 */
	public function viewUser() {
		$this->load->view('company/view_user', array('user' => 'active'));
	}

	/**
	 *  Get All User List
	 */
	public function getUserList() {
		$data['data'] = array();
		$data['status'] = 'empty';
		$data['recordsFiltered'] = 0;
		$data['recordsTotal'] = 0;
		$data['draw'] = $this->input->post('draw');

		$where = "";
		$start = $this->input->post('start');
		$length = $this->input->post('length');

		$search = $this->input->post('search');
		if(isset($search['value']) && !empty($search['value']))
			$where .= " AND ( su.id = '" . $search['value'] . "' OR su.name LIKE '%" . $search['value'] . "%' OR su.supervisor_id LIKE '%" . $search['value'] . "%' OR su.phone LIKE '%" . $search['value'] . "%' OR su.lname LIKE '%" . $search['value'] . "%' OR su.astatus LIKE '%" . $search['value'] . "%' OR su.wphone LIKE '%" . $search['value'] . "%' OR au.email LIKE '%" . $search['value'] . "%' OR ur.name LIKE '%" . $search['value'] . "%' ) ";

		if ($this->session->userdata('company')) {
			if ($this->session->userdata('company')['role'] == 2) {
				$company_id = $this->session->userdata('company')['company_id'];
				if(isset($company_id) && !empty($company_id) && $company_id != 0) {
					$sql = "SELECT su.id, su.name, su.supervisor_id, su.phone, su.lname, su.astatus, su.wphone, au.email, ur.name as user_role1, su.is_deleted FROM " . $this->_web . $this->_mast . "user AS su JOIN " . $this->_web . $this->_mast . "auth_user AS au ON au.id = su.user_id JOIN " . $this->_web . $this->_map . "user_role AS ur ON ur.id = su.user_role WHERE su.is_deleted = 0 AND su.company_id = '" . $company_id . "' " . $where;

					$getSqlCnt = $this->common->getRawQueryResult($sql);

					$sql .= " ORDER BY su.id DESC LIMIT " . $start . ", " . $length;

					$getSqlList = $this->common->getRawQueryResult($sql);
					
					if (count($getSqlList) > 0) {
						$data['draw'] = $this->input->post('draw');
						$data['recordsFiltered'] = count($getSqlCnt);
						$data['recordsTotal'] = count($getSqlCnt);
						$data['data'] = array();
						$no = $start;
						foreach ($getSqlList as $k) {
							$no++;
							$row = array();
							$row[] = $no;
							$row[] = $k['supervisor_id'];
							$row[] = $k['name'];
							$row[] = $k['user_role1'];
							$row[] = $k['name'].' '.$k['lname'];
							$row[] = $k['phone'];
							$row[] = $k['wphone'];
							$row[] = $k['email'];

							$status = array('0' => '-', '1' => 'Active', '2' => 'Terminated', '3' => 'Rehired');
							$row[] = (isset($k['astatus']) && !empty($k['astatus']) ? $status[$k['astatus']] : "-");

							$action = "";

							/*$action .= '<a href="forceLogin_supervisor.php?sid=<?php echo $supervisor['id']; ?>" data-toggle="tooltip" title="Sign As User"><i class="fa fa-sign-in" style="font-size:18px;color:#0056B3;"></i></a>
							<a href="portal_updatesupervisor.php?sid=<?php echo $supervisor['id'];?>" data-toggle="tooltip" title="Update User"><i class="fa fa-edit" style="font-size:18px;color:#008000;"></i></a>
                            <a onclick="delete_id(<?php echo $supervisor['id'];?>)" data-toggle="tooltip" title="Delete User"><i class="fa fa-trash-o" style="font-size:18px;color:#FF0000;"></i></a>';*/

							$action .= '<a data-toggle="tooltip" title="Switch User" class="switch_user" data-user_id="'.$k['id'].'" data-company_id="'.$company_id.'" data-user_type="3"><i class="fa fa-sign-in-alt" style="font-size:18px; color:#008000;"></i></a> <br>';
							$action .= '<a href="#" data-toggle="tooltip" title="Update User"><i class="fa fa-edit" style="font-size:18px; color:#001000;"></i></a> <br>';

                            if($k['is_deleted'] == '1')
								$action .= '<a data-toggle="tooltip" title="Activate User" class="px-1 update_user_status" data-user_status="0" data-user_id="'.$k['id'].'"><i class="fa fa-eye" style="font-size:18px; color:#339933;"></i></a>';
                            else
                                $action .= '<a data-toggle="tooltip" title="Delete User" class="px-1 update_user_status" data-user_status="1" data-user_id="'.$k['id'].'"><i class="fa fa-eye-slash" style="font-size:18px; color:#ff0000;"></i></a>';

                            $row[] = $action;
							
							$data['data'][] = $row;
						}
						$data['status'] = 'success';
					} else
						$data['status'] = 'empty';
				} else
					$data['status'] = 'Company info not found';
			} else 
				$data['status'] = 'User role not matched to this class';
		} else 
			$data['status'] = 'Session not found';

		echo json_encode($data);
	}

	/**
	 *  Delete User
	 */
	public function deleteUser() {
		$data = array('is_deleted' => $this->input->post('status'));
		$cond_1 = array('id' => $this->input->post('id'));
		$table_name_1 = $this->_web.$this->_mast.'user';
		$this->common->update($data, $cond_1, $table_name_1);

		$cond_2 = array('super_id' => $this->input->post('id'));
		$table_name_2 = $this->_web.$this->_mast.'notification';
		$this->common->delete($cond_2, $table_name_2);

		if($this->input->post('status') == '0')
			$json_data = array('status' => true, 'status_text' => 'User details activated');
		else
			$json_data = array('status' => true, 'status_text' => 'User details deleted');

		echo json_encode($json_data);
	}

	/**
	 *  Switch User
	 */
	public function switchUser() {
		$json_data = array();
		$json_data['status'] = false;
		$json_data['url'] = "";
		if ($this->session->userdata('company')) {
			if ($this->session->userdata('company')['role'] == 2) {
				$company_id = $this->session->userdata('company')['company_id'];
				if(isset($company_id) && !empty($company_id) && $company_id != 0) {
					$this->session->unset_userdata('user');

					$user_id = $this->input->post('id');
					$company_id = $this->input->post('company_id');
					$user_type = $this->input->post('user_type');

					$session_name = get_session_name_by_role($user_type);

					$user_sql = 'SELECT s.company_id AS cid, s.name, s.lname, au.username, c.name AS cname, c.speed_unit, c.fuel_unit, tz.sform, tz.timezone, ur.modules, ur.sub_modules FROM ' . $this->_web . $this->_mast . 'user AS s JOIN ' . $this->_web . $this->_mast . 'auth_user AS au ON au.id = IFNULL(s.user_id, 0) JOIN ' . $this->_web . $this->_mast . 'company AS c ON c.id = s.company_id JOIN ' . $this->_web . $this->_mast . 'timezonelist AS tz ON tz.id = c.timezone JOIN ' . $this->_web . $this->_map . 'user_role AS ur ON ur.id = IFNULL(s.user_role, 0) WHERE s.id = ' . $user_id . ' AND s.status = 0 AND s.is_deleted = 0';

					$user_row = $this->common->getRawQueryResult($user_sql);
					if(count($user_row) > 0) {
						$user_row = $user_row[0];
						$user_data = array(
							'id'     		=> $user_id,
							'cid'			=> $user_row['cid'],
							'name' 			=> $user_row['sub_modules'],
							'username'  	=> $user_row['username'],
							'role'			=> 3,
							'usertype'		=> 3,
							'email'     	=> "",
							'activeuid'		=> $user_id,
							'timezone'		=> $user_row['timezone'],
							'lname' 		=> $user_row['lname'],
							'sform'			=> $user_row['sform'],
							'speed_unit'	=> $user_row['speed_unit'],
							'fuel_unit'		=> $user_row['fuel_unit'],
							'company_id'	=> $user_row['cid'],
							'modules'		=> $user_row['modules'],
							'sub_modules'	=> $user_row['sub_modules'],
							'logged_in' 	=> true
						);

						$this->session->set_userdata($session_name, $user_data);

						$json_data['status'] = true;
						$json_data['status_text'] = 'Switch to user';
						$json_data['url'] = base_url().'user/home';
					} else
						$json_data['status_text'] = 'User info not found';
				} else
					$json_data['status_text'] = 'Company info not found';
			} else
				$json_data['status_text'] = 'User role not matched to this class';
		} else
			$json_data['status_text'] = 'Session not found';
		
		echo json_encode($json_data);
	}
	/********************************************/
	/**********************		USER MODULE ENDS	 **********************/
	/********************************************/



	/********************************************/
	/**********************		ROLE MODULE STARTS	 **********************/
	/********************************************/
	/**
	 *  Render Role Add Page
	 */
	public function addRole() {
		$this->load->view('company/add_role', array('role' => 'active'));
	}

	/**
	 *  Save New Role
	 */
	public function saveRole() {
		$data['data'] = array();
		if ($this->session->userdata('company')) {
			if ($this->session->userdata('company')['role'] == 2) {
				$company_id = $this->session->userdata('company')['company_id'];
				if(isset($company_id) && !empty($company_id) && $company_id != 0) {
					$role_name = $this->input->post('role_name') ? $this->input->post('role_name') : "";
					$module_access = $this->input->post('chk') ? $this->input->post('chk') : "";
					$sub_module_access = $this->input->post('sub_chk') ? $this->input->post('sub_chk') : "";
					$modules = "";
					$sub_modules = "";

					$field = 'id';
					$table_name = $this->_web . $this->_map . "user_role";
					$cond = array('name' => $role_name, 'is_deleted' => 0, 'company_id' => $company_id);
					$getRoleCnt = $this->common->getTotalCount($table_name, $field, $cond);

					if($getRoleCnt > 0) {
						$data['status'] = false;
						$data['status_text'] = 'Role name already exists. Try different role name';
					} else {
						if (!empty($module_access))
							$modules = implode(",", $module_access);
						if (!empty($sub_module_access))
							$sub_modules = implode(",", $sub_module_access);

						$data = array(
							'name' => $role_name,
							'modules' => $modules,
							'sub_modules' => $sub_modules,
							'company_id' => $company_id,
							'created_date' => time(),
							'is_deleted' => 0
						);
						$this->common->insert($data, $table_name);

						$data['status'] = true;
						$data['status_text'] = 'Role added';
					}
				} else {
					$data['status'] = false;
					$data['status_text'] = 'Company info not found';
				}
			} else {
				$data['status'] = false;
				$data['status_text'] = 'User role not matched to this class';
			}
		} else {
			$data['status'] = false;
			$data['status_text'] = 'Session not found';
		}
		echo json_encode($data);
	}

	/**
	 *  Render Role Edit Page
	 */
	public function editRole($id = '') {
		//$role_id = dec($id);
		$role_id = decode(strtolower($id));
		$role_id = ($role_id != "" && $role_id != 0 ? $role_id : 0);

		$data = array();
		$data['role_id'] = $role_id;
		$data['role_detail'] = [];
		if($role_id != "" && $role_id != 0) {
			$table_name = $this->_web . $this->_map . "user_role";
			$field = '*';
			$flag = 1; // 0 - Row Array, 1 - Result Array
			$cond = array('id' => $role_id);
			$role_detail = $this->common->fetchData($table_name, $field, $cond, '1', $flag);
		}
		$this->load->view('company/edit_role', array(
			'role_detail' => count($role_detail) > 0 ? $role_detail[0] : [],
			'role_id' => $role_id,
			'role' => 'active'
		));
	}

	/**
	 *  Update Role
	 */
	public function updateRole() {
		$data['data'] = array();
		if ($this->session->userdata('company')) {
			if ($this->session->userdata('company')['role'] == 2) {
				$company_id = $this->session->userdata('company')['company_id'];
				if(isset($company_id) && !empty($company_id) && $company_id != 0) {
					$role_id = $this->input->post('role_id') ? $this->input->post('role_id') : "";
					$role_name = $this->input->post('role_name') ? $this->input->post('role_name') : "";
					$module_access = $this->input->post('chk') ? $this->input->post('chk') : "";
					$sub_module_access = $this->input->post('sub_chk') ? $this->input->post('sub_chk') : "";
					$modules = "";
					$sub_modules = "";
					
					if($role_id != 0) {
						if (!empty($module_access))
							$modules = implode(",", $module_access);
						if (!empty($sub_module_access))
							$sub_modules = implode(",", $sub_module_access);

						$table_name = $this->_web . $this->_map . "user_role";
						$cond = array('id' => $role_id);
						$data = array(
							'name' => $role_name,
							'modules' => $modules,
							'sub_modules' => $sub_modules,
							'company_id' => $company_id,
							'created_date' => time(),
							'is_deleted' => 0
						);
						$this->common->update($data, $cond, $table_name);

						$data['status'] = true;
						$data['status_text'] = 'Role updated';
					} else {
						$data['status'] = false;
						$data['status_text'] = 'Role id not found';
					}
				} else {
					$data['status'] = false;
					$data['status_text'] = 'Company info not found';
				}
			} else {
				$data['status'] = false;
				$data['status_text'] = 'User role not matched to this class';
			}
		} else {
			$data['status'] = false;
			$data['status_text'] = 'Session not found';
		}
		echo json_encode($data);
	}

	/**
	 *  Render Role View Page
	 */
	public function viewRole() {
		$this->load->view('company/view_role', array('role' => 'active'));
	}

	/**
	 *  Get All Role List
	 */
	public function getRoleList() {
		$data['data'] = array();
		$data['status'] = 'empty';
		$data['recordsFiltered'] = 0;
		$data['recordsTotal'] = 0;
		$data['draw'] = $this->input->post('draw');

		$where = "";
		$start = $this->input->post('start');
		$length = $this->input->post('length');

		$search = $this->input->post('search');
		if(isset($search['value']) && !empty($search['value']))
			$where .= " AND su.name LIKE '%" . $search['value'] . "%' ";

		if ($this->session->userdata('company')) {
			if ($this->session->userdata('company')['role'] == 2) {
				$company_id = $this->session->userdata('company')['company_id'];
				if(isset($company_id) && !empty($company_id) && $company_id != 0) {
					$sql = "SELECT id, name FROM " . $this->_web . $this->_map . "user_role WHERE is_deleted = 0 AND company_id = '" . $company_id . "' " . $where;

					$getSqlCnt = $this->common->getRawQueryResult($sql);

					$sql .= " ORDER BY id DESC LIMIT " . $start . ", " . $length;

					$getSqlList = $this->common->getRawQueryResult($sql);
					
					if (count($getSqlList) > 0) {
						$data['draw'] = $this->input->post('draw');
						$data['recordsFiltered'] = count($getSqlCnt);
						$data['recordsTotal'] = count($getSqlCnt);
						$data['data'] = array();
						$no = $start;
						foreach ($getSqlList as $k) {
							$no++;
							$row = array();
							$row[] = $no;
							$row[] = $k['name'];
							$action = "";
							$action .= '<a href="'.base_url().'company/editRole/' . strtolower(encode($k['id'])) . '" data-toggle="tooltip" title="Update Role"><i class="fa fa-edit" style="font-size:18px; color:#008000;"></i></a> ';
                            $action .= '<a data-toggle="tooltip" title="Delete Role" class="px-1 update_role_status" data-role_status="1" data-role_id="'.$k['id'].'"><i class="fa fa-trash" style="font-size:18px; color:#FF0000;"></i></a>';

                            $row[] = $action;
							
							$data['data'][] = $row;
						}
						$data['status'] = 'success';
					} else
						$data['status'] = 'empty';
				} else
					$data['status'] = 'Company info not found';
			} else 
				$data['status'] = 'User role not matched to this class';
		} else 
			$data['status'] = 'Session not found';

		echo json_encode($data);
	}

	/**
	 *  Delete Role
	 */
	public function deleteRole()
	{
		$data = array('is_deleted' => $this->input->post('status'));
		$cond = array('id' => $this->input->post('id'));
		$table_name = $this->_web.$this->_map.'user_role';
		$this->common->update($data, $cond, $table_name);

		if($this->input->post('status') == '0')
			$json_data = array('status' => true, 'status_text' => 'Role details activated');
		else
			$json_data = array('status' => true, 'status_text' => 'Role details deleted');

		echo json_encode($json_data);
	}
	/********************************************/
	/**********************		ROLE MODULE ENDS	 **********************/
	/********************************************/




	/**
	 *  Add driver Page
	 */
	public function add_driver() {
		$this->load->view('company/add_driver');
	}
	public function create_driver() {

			$company_id = $this->session->userdata('company')['company_id'];

		// $this->db->select("*");
		// $this->db->from('web_mast_driver');
		// $this->db->where('company_id', $company_id);
		// $this->db->where('is_deleted', 0);
		// $query = $this->db->get();
		// $duplicate = $query->num_rows();
		// $duplicate = $query->row_array();
		// if ($duplicate) {
		// 	# code...
		// }
// print_r($duplicate);
$username = $this->input->post('username');
			   $query = $this->db->query("select count(*) as total from web_mast_auth_user where username = '$username' AND is_deleted = '0'");
               $resc = $query->result_array();


		if ($resc[0]['total'] < 0) {
		
		$date = date('Y-m-d h:i:s');
		$data = array('username'=>$this->input->post('username'),
			'email'=>$this->input->post('mailid'),
			'password'=>$this->input->post('password'),
			'is_staff'=>0,
			'date_joined'=>$date,
			'usertype'=>4);
	    	$this->db->insert('web_mast_auth_user',$data);
			// $last_id =  $this->get_lastid();
			$last_id = $this->db->insert_id();

			$ctime = date("Y-m-d H:i:s.u");
		

			$data1 = array('name'=>$this->input->post('username'),
			'driver_id'=>$this->input->post('driver_id'),
			'phone'=>$this->input->post('phone'),
			'driver_type'=>$this->input->post('drivertype'),
			'contractor'=>$this->input->post('contractor'),
			'license_no'=>$this->input->post('license_no'),
			'license_province'=>$this->input->post('license_province'),
			'terminal'=>$this->input->post('terminal'),
			'cdn_cycle'=>$this->input->post('cdn_cycle'),
			'us_cycle'=>$this->input->post('us_cycle'),
			'company_id'=>$company_id,
			'entry_time'=>$ctime,
			'user_id'=>$last_id,
			'status'=> 0,
			'lastname'=>$this->input->post('lastname'),
			'dob'=>$this->input->post('dob'),
			'citizenship'=>$this->input->post('citizenship'),
			'address'=>$this->input->post('address'),
			'postalcode'=>$this->input->post('postalcode'),
			'mobile2'=>$this->input->post('mobile2'),
			'license_issue_date'=>$this->input->post('license_issue_date'),
			'license_expiry_date'=>$this->input->post('license_expiry_date'),
			'medical_expiry_date'=>$this->input->post('medical_expiry_date'),
			'hire_date'=>$this->input->post('hire_date'),
			'hr_start_period'=>$this->input->post('hr_start_period'),
			'state'=>$this->input->post('state'),
			'hazmat_certified'=>$this->input->post('hazmat_certified'),
			'fast'=>$this->input->post('fast'),
			'lcv'=>$this->input->post('lcv'),
			'home_terminal'=>$this->input->post('home_terminal'),
			'doi'=>$this->input->post('doi'),
			'doe'=>$this->input->post('doe'),
			'f_doi'=>$this->input->post('f_doi'));
		$this->db->insert('web_mast_driver',$data1);

		if ($data1) {
	     $json_data = array('status' => true, 'status_text' => 'Driver Added');

		echo json_encode($json_data);
		exit();
		}
		else
		{
		 $json_data = array('status' => false, 'status_text' => 'something went wrong');

		echo json_encode($json_data);
		exit();
		}

	}
	else{
		 $json_data = array('status' => false, 'status_text' => 'Username already exists');

		echo json_encode($json_data);
		exit();
	}
	}

// Edit driver
    public function edit_driver() {
    	echo "<pre>";
print_r($this->session->all_userdata());
    // 	$cond = array('company_id' => $this->session->userdata('company')['company_id'],'is_deleted' => 0);
				// $data['truck_count'] = $this->common->fetchData('web_mast_driver', 'id', $cond, '', 1);


		// $this->load->view('company/edit_driver',$data);
	}


	/********************************************/
	/**********************		ASSIGN DEVICE MODULE STARTS	 **********************/
	/********************************************/
	/**
	 *  Render Assigned Devices View Page
	 */
	public function viewAssignedDevice() {
		$data = array();
		$data['truck_detail'] = [];
		$data['trailer_detail'] = [];
		if ($this->session->userdata('company')) {
			if ($this->session->userdata('company')['role'] == 2) {
				$company_id = $this->session->userdata('company')['company_id'];
				if(isset($company_id) && !empty($company_id) && $company_id != 0) {
					if($company_id != "" && $company_id != 0) {
						$table_name_1 = $this->_web . $this->_mast . "truck";
						$field = 'id, unit_no';
						$flag = 1; // 0 - Row Array, 1 - Result Array
						$cond = array('company_id' => $company_id, 'is_deleted' => 0, 'LENGTH(device_id)' =>0);
						$truck_detail = $this->common->fetchData($table_name_1, $field, $cond, '', $flag);

						$table_name_2 = $this->_web . $this->_mast. "trailer";
						$trailer_detail = $this->common->fetchData($table_name_2, $field, $cond, '', $flag);
					} else
						redirect('auth/signin');
				} else
					redirect('auth/signin');
			} else
				redirect('auth/signin');
		} else
			redirect('auth/signin');
				
		$this->load->view('company/view_assign_devices', array(
			'truck_detail' => count($truck_detail) > 0 ? $truck_detail : [],
			'trailer_detail' => count($trailer_detail) > 0 ? $trailer_detail : [],
			'company_id' => $company_id,
			'device' => 'active'
		));
	}

	/**
	 *  Get All Assigned Devices List
	 */
	public function getAssignedDeviceList() {
		$data['data'] = array();
		$data['status'] = 'empty';
		$data['recordsFiltered'] = 0;
		$data['recordsTotal'] = 0;
		$data['draw'] = $this->input->post('draw');

		$where = "";
		$start = $this->input->post('start');
		$length = $this->input->post('length');

		$device_status = $this->input->post('device_status');
		$vehicle_type = $this->input->post('vehicle_type');

		$search = $this->input->post('search');
		if(isset($search['value']) && !empty($search['value']))
			$where .= " AND ( ge.devicename LIKE '%" . $search['value'] . "%' OR ge.ecm_gps_number LIKE '%" . $search['value'] . "%' OR ge.imei_number LIKE '%" . $search['value'] . "%' OR ge.sim LIKE '%" . $search['value'] . "%' OR cg.gps_deviceid LIKE '%" . $search['value'] . "%' OR vt.type LIKE '%" . $search['value'] . "%' OR tk.name LIKE '%" . $search['value'] . "%' OR tr.name LIKE '%" . $search['value'] . "%' OR tk.unit_no LIKE '%" . $search['value'] . "%' OR tr.unit_no LIKE '%" . $search['value'] . "%' OR tk.plate_no LIKE '%" . $search['value'] . "%' OR tr.plate_no LIKE '%" . $search['value'] . "%' OR tk.vin_no LIKE '%" . $search['value'] . "%' OR tr.vin_no LIKE '%" . $search['value'] . "%' ) ";

		if($device_status != "") {
			if($device_status == 0) {
				$where .= ' AND (CASE WHEN IFNULL((SELECT COUNT(id) FROM ' . $this->_web . $this->_mast . 'truck where device_id = cg.gps_deviceid), 0) > 0 THEN 1 ELSE CASE WHEN IFNULL((SELECT COUNT(id) FROM ' . $this->_web . $this->_mast . 'trailer where device_id = cg.gps_deviceid), 0) > 0 THEN 2 ELSE 0 END END) = 0 ';
			} else if($device_status == 1) {
				$where .= ' AND (CASE WHEN IFNULL((SELECT COUNT(id) FROM ' . $this->_web . $this->_mast . 'truck where device_id = cg.gps_deviceid), 0) > 0 THEN 1 ELSE CASE WHEN IFNULL((SELECT COUNT(id) FROM ' . $this->_web . $this->_mast . 'trailer where device_id = cg.gps_deviceid), 0) > 0 THEN 2 ELSE 0 END END) > 0 ';
			}
		}

		if($vehicle_type != "") {
			$where .= ' AND CASE WHEN IFNULL((SELECT COUNT(id) FROM ' . $this->_web . $this->_mast . 'truck WHERE device_id = cg.gps_deviceid), 0) > 0 THEN 1 ELSE CASE WHEN IFNULL((SELECT COUNT(id) FROM ' . $this->_web . $this->_mast . 'trailer WHERE device_id = cg.gps_deviceid), 0) > 0 THEN 2 ELSE 0 END END = ' . $vehicle_type;
		}

		if ($this->session->userdata('company')) {
			if ($this->session->userdata('company')['role'] == 2) {
				$company_id = $this->session->userdata('company')['company_id'];
				if(isset($company_id) && !empty($company_id) && $company_id != 0) {
						$sql = 'SELECT ge.devicename, ge.ecm_gps_number, ge.imei_number, ge.sim, cg.gps_deviceid, vt.type, CASE WHEN IFNULL(tk.name, "") = "" THEN CASE WHEN IFNULL(tr.name, "") = "" THEN "" ELSE IFNULL(tr.name, "") END ELSE IFNULL(tk.name, "") END AS name, CASE WHEN IFNULL(tk.unit_no, "") = "" THEN CASE WHEN IFNULL(tr.unit_no, "") = "" THEN "" ELSE IFNULL(tr.unit_no, "") END ELSE IFNULL(tk.unit_no, "") END AS unit_no, CASE WHEN IFNULL(tk.plate_no, "") = "" THEN CASE WHEN IFNULL(tr.plate_no, "") = "" THEN "" ELSE IFNULL(tr.plate_no, "") END ELSE IFNULL(tk.plate_no, "") END AS plate_no, CASE WHEN IFNULL(tk.vin_no, "") = "" THEN CASE WHEN IFNULL(tr.vin_no, "") = "" THEN "" ELSE IFNULL(tr.vin_no, "") END ELSE IFNULL(tk.vin_no, "") END AS vin_no, CASE WHEN IFNULL((SELECT COUNT(id) FROM ' . $this->_web . $this->_mast . 'truck where device_id = cg.gps_deviceid), 0) > 0 THEN 1 ELSE CASE WHEN IFNULL((SELECT COUNT(id) FROM ' . $this->_web . $this->_mast . 'trailer where device_id = cg.gps_deviceid), 0) > 0 THEN 2 ELSE 0 END END AS tname, CASE WHEN IFNULL((SELECT COUNT(id) FROM ' . $this->_web . $this->_mast . 'truck WHERE device_id = cg.gps_deviceid), 0) > 0 THEN "truck" ELSE CASE WHEN IFNULL((SELECT COUNT(id) FROM ' . $this->_web . $this->_mast . 'trailer WHERE device_id = cg.gps_deviceid), 0) > 0 THEN "trailer" ELSE "" END END AS device_mode, CASE WHEN IFNULL((SELECT id FROM ' . $this->_web . $this->_mast . 'truck WHERE device_id = cg.gps_deviceid LIMIT 1), 0) > 0 THEN IFNULL((SELECT id FROM ' . $this->_web . $this->_mast . 'truck WHERE device_id = cg.gps_deviceid LIMIT 1), 0) ELSE CASE WHEN IFNULL((SELECT id FROM ' . $this->_web . $this->_mast . 'trailer WHERE device_id = cg.gps_deviceid LIMIT 1), 0) > 0 THEN IFNULL((SELECT id FROM ' . $this->_web . $this->_mast . 'trailer WHERE device_id = cg.gps_deviceid LIMIT 1), 0) ELSE 0 END END AS truck_trailer_id FROM ' . $this->_web . $this->_map . 'company_device AS cg INNER JOIN ' . $this->_web . $this->_mast . 'device AS ge ON ge.id = cg.gps_deviceid JOIN ' . $this->_web . $this->_mast . 'vehical_type AS vt ON vt.id = ge.type_of_device LEFT JOIN ' . $this->_web . $this->_mast . 'truck AS tk ON tk.device_id = cg.gps_deviceid LEFT JOIN ' . $this->_web . $this->_mast . 'trailer AS tr ON tr.device_id = cg.gps_deviceid WHERE cg.company_id = "' . $company_id . '" AND ge.is_deleted = 0 ' . $where;

					$getSqlCnt = $this->common->getRawQueryResult($sql);

					$sql .= " LIMIT " . $start . ", " . $length;

					$getSqlList = $this->common->getRawQueryResult($sql);
					
					if (count($getSqlList) > 0) {
						$data['draw'] = $this->input->post('draw');
						$data['recordsFiltered'] = count($getSqlCnt);
						$data['recordsTotal'] = count($getSqlCnt);
						$data['data'] = array();
						$no = $start;
						foreach ($getSqlList as $k) {
							$no++;
							$row = array();
							$row[] = $no;
							$row[] = $k['devicename'];
							$row[] = $k['type'];
							//$row[] = ($k['unit_no'] != "" ? $k['unit_no'] : "Unassigned");
							$row[] = $k['unit_no'];
							$row[] = $k['name'];
							$row[] = $k['plate_no'];
							$row[] = $k['ecm_gps_number'];
							$row[] = $k['imei_number'];
							$row[] = $k['sim'];

							$vehicle_mode = ['Others', 'Truck', 'Trailer'];
							$row[] = $vehicle_mode[$k['tname']];

							$row[] = (!empty($k['unit_no']) && $k['unit_no'] != "" && $k['unit_no'] != "0" ? "Assigned" : "Not Assigned");
                            
							$action = "";
                            if(!empty($k['unit_no']) && $k['unit_no'] != "" && $k['unit_no'] != "0") {
								$action .= '<a data-toggle="tooltip" title="Unassign Device" class="px-1 unassign_device" data-device_id="'.$k['gps_deviceid'].'" data-device_type="'.$k['tname'].'" data-device_mode="'.$k['device_mode'].'" data-truck_trailer_id="'.$k['truck_trailer_id'].'"><i class="fa fa-user-times" style="font-size:18px; color:#FF0000;"></i></a>';
                            } else {
                                $action .= '<a data-toggle="tooltip" title="Assign Device" class="px-1 assign_device" data-device_id="'.$k['gps_deviceid'].'" data-device_type="'.$k['tname'].'"><i class="fa fa-user-plus" style="font-size:18px; color:#008000;"></i></a>';
                            }

                            $row[] = $action;
							
							$data['data'][] = $row;
						}
						$data['status'] = 'success';
					} else
						$data['status'] = 'empty';
				} else
					$data['status'] = 'Company info not found';
			} else 
				$data['status'] = 'User role not matched to this class';
		} else 
			$data['status'] = 'Session not found';

		echo json_encode($data);
	}

	/**
	 *	Get Device Info
	 */
	public function getDeviceInfo() {
		$json_data = array();
		$json_data['status'] = false;
		if ($this->session->userdata('company')) {
			if ($this->session->userdata('company')['role'] == 2) {
				$company_id = $this->session->userdata('company')['company_id'];
				$device_id = $this->input->post('id');
				if(isset($company_id) && !empty($company_id) && $company_id != 0) {
					$sql = "SELECT ge.id, ge.device_id, ge.devicename FROM " . $this->_web . $this->_map . "company_device AS cg INNER JOIN " . $this->_web . $this->_mast . "device AS ge ON ge.id = cg.gps_deviceid WHERE cg.company_id = '" . $company_id . "' AND ge.is_deleted = 0 AND ge.id = '". $device_id . "' ";

					$getSqlList = $this->common->getRawQueryResult($sql);
					if (count($getSqlList) > 0) {
						$json_data['status'] = true;
						$json_data['device_info'] = $getSqlList[0];
					}
				}
			}
		}

		echo json_encode($json_data);
	}

	/**
	 *	Save Assign Device Info
	 */
	public function saveAssignDeviceInfo() {
		$json_data = array();
		$json_data['status'] = false;
		$json_data['status_text'] = 'Failed to assign the device';
		if ($this->session->userdata('company')) {
			if ($this->session->userdata('company')['role'] == 2) {
				$company_id = $this->session->userdata('company')['company_id'];
				if(isset($company_id) && !empty($company_id) && $company_id != 0) {
					$device_id = $this->input->post('device_id');
					$device_type = $this->input->post('device_type');
					$device_mode = $this->input->post('device_mode');
					$truck_id = $this->input->post('truck_id');
					$trailer_id = $this->input->post('trailer_id');

					$vehicle_id = $device_mode == 'truck' ? $truck_id : $trailer_id;
					$data_1 = array('device_id' => $device_id);
					$cond = array('id' => $vehicle_id);
					$table_name_1 = $this->_web.$this->_mast.strtolower($device_mode);
					$this->common->update($data_1, $cond, $table_name_1);

					$table_name_2 = $this->_web.$this->_map.'device_history';
					$data_2 = array(
						'device_id' => $device_id,
						'veh_type' => ucfirst($device_mode),
						'veh_id' => $vehicle_id,
						'date_time' => convert_date(),
						'status' => 'Assign',
						'company_id' => $company_id
					);
					$this->common->insert($data_2, $table_name_2);

					$json_data['status'] = true;
					$json_data['status_text'] = 'Device assigned';
				}
			}
		}

		echo json_encode($json_data);
	}

	/**
	 *	Remove Assign Device Info
	 */
	public function removeAssignDeviceInfo() {
		$json_data = array();
		$json_data['status'] = false;
		$json_data['status_text'] = 'Failed to unassign the device';
		if ($this->session->userdata('company')) {
			if ($this->session->userdata('company')['role'] == 2) {
				$company_id = $this->session->userdata('company')['company_id'];
				if(isset($company_id) && !empty($company_id) && $company_id != 0) {
					$id = $this->input->post('id');
					$device_id = $this->input->post('device_id');
					$device_type = $this->input->post('device_type');
					$device_mode = $this->input->post('device_mode');

					$cond = array('id' => $id);
					$data_1 = array('device_id' => "");
					$table_name_1 = $this->_web.$this->_mast.strtolower($device_mode);
					$this->common->update($data_1, $cond, $table_name_1);

					$table_name_2 = $this->_web.$this->_map.'device_history';
					$data_2 = array(
						'device_id' => $device_id,
						'veh_type' => ucfirst($device_mode),
						'veh_id' => $id,
						'date_time' => convert_date(),
						'status' => 'Unassign',
						'company_id' => $company_id
					);
					$this->common->insert($data_2, $table_name_2);

					$json_data['status'] = true;
					$json_data['status_text'] = 'Device unassigned';
				}
			}
		}

		echo json_encode($json_data);
	}




	/********************************************/
	/**********************		TRUCK MODULE STARTS	 **********************/
	/********************************************/
	/**
	 *  Render Truck View Page
	 */
	public function viewTruck() {
		$this->load->view('company/view_truck', array('truck' => 'active'));
	}

	/**
	 *  Get All Truck List
	 */
	public function getTruckList() {
		$data['data'] = array();
		$data['status'] = 'empty';
		$data['recordsFiltered'] = 0;
		$data['recordsTotal'] = 0;
		$data['draw'] = $this->input->post('draw');

		$where = "";
		$start = $this->input->post('start');
		$length = $this->input->post('length');

		$truck_status = $this->input->post('truck_status');

		$search = $this->input->post('search');
		if(isset($search['value']) && !empty($search['value']))
			$where .= " AND ( t.name = '" . $search['value'] . "' OR t.unit_no LIKE '%" . $search['value'] . "%' OR t.plate_no LIKE '%" . $search['value'] . "%' OR t.vin_no LIKE '%" . $search['value'] . "%' OR t.plate_expiry LIKE '%" . $search['value'] . "%' OR t.fuel_capacity LIKE '%" . $search['value'] . "%' OR t.fuel_capacity_unit LIKE '%" . $search['value'] . "%' OR t.device_id LIKE '%" . $search['value'] . "%' OR t.make LIKE '%" . $search['value'] . "%' OR m.devicename LIKE '%" . $search['value'] . "%' ) ";

		if($truck_status != "") {
			if($truck_status == "0")
				$where .= " AND IFNULL(m.devicename, '') = '' ";

			if($truck_status == "1")
				$where .= " AND IFNULL(m.devicename, '') != '' ";
		}

		if ($this->session->userdata('company')) {
			if ($this->session->userdata('company')['role'] == 2) {
				$company_id = $this->session->userdata('company')['company_id'];
				if(isset($company_id) && !empty($company_id) && $company_id != 0) {
					$sql = "SELECT t.id, t.name, t.unit_no, t.plate_no, t.vin_no, t.plate_expiry, t.fuel_capacity, t.fuel_capacity_unit, t.device_id, t.make, IFNULL(m.devicename, '') AS devicename FROM " . $this->_web . $this->_mast . "truck AS t LEFT JOIN " . $this->_web . $this->_mast . "device AS m ON m.id = t.device_id WHERE t.is_deleted = 0 AND t.company_id = '" . $company_id . "' " . $where;

					$getSqlCnt = $this->common->getRawQueryResult($sql);

					$sql .= " ORDER BY t.id DESC LIMIT " . $start . ", " . $length;

					$getSqlList = $this->common->getRawQueryResult($sql);
					
					if (count($getSqlList) > 0) {
						$data['draw'] = $this->input->post('draw');
						$data['recordsFiltered'] = count($getSqlCnt);
						$data['recordsTotal'] = count($getSqlCnt);
						$data['data'] = array();
						$no = $start;
						foreach ($getSqlList as $k) {
							$no++;
							$row = array();
							$row[] = $no;
							$row[] = $k['vin_no'];
							$row[] = $k['make'] . '/' . $k['name'];
							$row[] = $k['plate_no'];
							$row[] = $k['unit_no'];
							$row[] = $k['plate_expiry'];
							$row[] = $k['devicename'] ? $k['devicename'] : "-";
							$row[] = $k['fuel_capacity'];
							$row[] = $k['fuel_capacity_unit'];

							$action = "";

							/*$action .= '<a href="portal_updatetruck.php?tid=<?php echo $truck['id'];?>" data-toggle="tooltip" title="Update Truck"><i class="fa fa-edit" style="font-size:18px;color:#008000;"></i></a>
							<a onclick="delete_id(<?php echo $truck['id'];?>)" data-toggle="tooltip" title="Delete Truck"><i class="fa fa-trash-o" style="font-size:18px;color:#FF0000;"></i></a>
							<a href="portal_history.php?tid=<?php echo $truck['id'];?>&nid=1" data-toggle="tooltip" title="History"><i class="fa fa-history" style="font-size:18px;color:#008000;"></i></a>*/

							$action .= '<a href="#" data-toggle="tooltip" title="Update Truck"><i class="fa fa-edit" style="font-size:18px; color:#001000;"></i></a>';
                            $action .= '<a data-toggle="tooltip" title="Delete Truck" class="px-1 update_truck_status" data-truck_status="1" data-truck_id="'.$k['id'].'"><i class="fa fa-eye-slash" style="font-size:18px; color:#ff0000;"></i></a>';
                            //$action .= '<a href="#" data-toggle="tooltip" title="Truck History" class="px-1"><i class="fa fa-history" style="font-size:18px;color:#008000;"></i></a>';

                            $row[] = $action;
							
							$data['data'][] = $row;
						}
						$data['status'] = 'success';
					} else
						$data['status'] = 'empty';
				} else
					$data['status'] = 'Company info not found';
			} else 
				$data['status'] = 'User role not matched to this class';
		} else 
			$data['status'] = 'Session not found';

		echo json_encode($data);
	}

	/**
	 *  Delete Truck
	 */
	public function deleteTruck() {
		$data = array('is_deleted' => $this->input->post('status'));
		$cond = array('id' => $this->input->post('id'));
		$table_name = $this->_web.$this->_mast.'truck';
		$this->common->update($data, $cond, $table_name);

		if($this->input->post('status') == '0')
			$json_data = array('status' => true, 'status_text' => 'Truck details activated');
		else
			$json_data = array('status' => true, 'status_text' => 'Truck details deleted');

		echo json_encode($json_data);
	}
	/********************************************/
	/**********************		TRUCK MODULE ENDS	 **********************/
	/********************************************/





	/********************************************/
	/**********************		TRAILER MODULE STARTS	 **********************/
	/********************************************/
	/**
	 *  Render Trailer View Page
	 */
	public function viewTrailer() {
		$this->load->view('company/view_trailer', array('trailer' => 'active'));
	}

	/**
	 *  Get All Trailer List
	 */
	public function getTrailerList() {
		$data['data'] = array();
		$data['status'] = 'empty';
		$data['recordsFiltered'] = 0;
		$data['recordsTotal'] = 0;
		$data['draw'] = $this->input->post('draw');

		$where = "";
		$start = $this->input->post('start');
		$length = $this->input->post('length');

		$trailer_status = $this->input->post('trailer_status');

		$search = $this->input->post('search');
		if(isset($search['value']) && !empty($search['value']))
			$where .= " AND ( t.name = '" . $search['value'] . "' OR t.unit_no LIKE '%" . $search['value'] . "%' OR t.plate_no LIKE '%" . $search['value'] . "%' OR t.vin_no LIKE '%" . $search['value'] . "%' OR t.plate_expiry LIKE '%" . $search['value'] . "%' OR t.fuel_capacity LIKE '%" . $search['value'] . "%' OR t.fuel_capacity_unit LIKE '%" . $search['value'] . "%' OR t.device_id LIKE '%" . $search['value'] . "%' OR t.make LIKE '%" . $search['value'] . "%' OR m.devicename LIKE '%" . $search['value'] . "%' ) ";

		if($trailer_status != "") {
			if($trailer_status == "0")
				$where .= " AND IFNULL(m.devicename, '') = '' ";

			if($trailer_status == "1")
				$where .= " AND IFNULL(m.devicename, '') != '' ";
		}

		if ($this->session->userdata('company')) {
			if ($this->session->userdata('company')['role'] == 2) {
				$company_id = $this->session->userdata('company')['company_id'];
				if(isset($company_id) && !empty($company_id) && $company_id != 0) {
					$sql = "SELECT t.id, t.name, t.unit_no, t.plate_no, t.vin_no, t.plate_expiry, t.fuel_capacity, t.fuel_capacity_unit, t.device_id, t.make, IFNULL(m.devicename, '') AS devicename FROM " . $this->_web . $this->_mast . "trailer AS t LEFT JOIN " . $this->_web . $this->_mast . "device AS m ON m.id = t.device_id WHERE t.is_deleted = 0 AND t.company_id = '" . $company_id . "' " . $where;

					$getSqlCnt = $this->common->getRawQueryResult($sql);

					$sql .= " ORDER BY t.id DESC LIMIT " . $start . ", " . $length;

					$getSqlList = $this->common->getRawQueryResult($sql);
					
					if (count($getSqlList) > 0) {
						$data['draw'] = $this->input->post('draw');
						$data['recordsFiltered'] = count($getSqlCnt);
						$data['recordsTotal'] = count($getSqlCnt);
						$data['data'] = array();
						$no = $start;
						foreach ($getSqlList as $k) {
							$no++;
							$row = array();
							$row[] = $no;
							$row[] = $k['vin_no'];
							$row[] = $k['make'] . '/' . $k['name'];
							$row[] = $k['plate_no'];
							$row[] = $k['unit_no'];
							$row[] = $k['plate_expiry'];
							$row[] = $k['devicename'] ? $k['devicename'] : "-";
							$row[] = $k['fuel_capacity'];
							$row[] = $k['fuel_capacity_unit'];

							$action = "";

							/*$action .= '<a href="portal_updatetruck.php?tid=<?php echo $truck['id'];?>" data-toggle="tooltip" title="Update Truck"><i class="fa fa-edit" style="font-size:18px;color:#008000;"></i></a>
							<a onclick="delete_id(<?php echo $truck['id'];?>)" data-toggle="tooltip" title="Delete Truck"><i class="fa fa-trash-o" style="font-size:18px;color:#FF0000;"></i></a>
							<a href="portal_history.php?tid=<?php echo $truck['id'];?>&nid=1" data-toggle="tooltip" title="History"><i class="fa fa-history" style="font-size:18px;color:#008000;"></i></a>*/

							$action .= '<a href="#" data-toggle="tooltip" title="Update Trailer"><i class="fa fa-edit" style="font-size:18px; color:#001000;"></i></a>';
                            $action .= '<a data-toggle="tooltip" title="Delete Trailer" class="px-1 update_trailer_status" data-trailer_status="1" data-trailer_id="'.$k['id'].'"><i class="fa fa-eye-slash" style="font-size:18px; color:#ff0000;"></i></a>';
                            //$action .= '<a href="#" data-toggle="tooltip" title="Trailer History" class="px-1"><i class="fa fa-history" style="font-size:18px;color:#008000;"></i></a>';

                            $row[] = $action;
							
							$data['data'][] = $row;
						}
						$data['status'] = 'success';
					} else
						$data['status'] = 'empty';
				} else
					$data['status'] = 'Company info not found';
			} else 
				$data['status'] = 'User role not matched to this class';
		} else 
			$data['status'] = 'Session not found';

		echo json_encode($data);
	}

	/**
	 *  Delete Trailer
	 */
	public function deleteTrailer() {
		$data = array('is_deleted' => $this->input->post('status'));
		$cond = array('id' => $this->input->post('id'));
		$table_name = $this->_web.$this->_mast.'trailer';
		$this->common->update($data, $cond, $table_name);

		if($this->input->post('status') == '0')
			$json_data = array('status' => true, 'status_text' => 'Trailer details activated');
		else
			$json_data = array('status' => true, 'status_text' => 'Trailer details deleted');

		echo json_encode($json_data);
	}
	/********************************************/
	/**********************		TRAILER MODULE ENDS	 **********************/
	/********************************************/





	/********************************************/
	/**********************		TRIP MODULE STARTS	 **********************/
	/********************************************/
	/**
	 *  Render Trip View Page
	 */
	public function viewTrip() {
		$this->load->view('company/view_trip', array('trip' => 'active'));
	}

	/**
	 *  Get All Trip List
	 */
	public function getTripList() {
		$data['data'] = array();
		$data['status'] = 'empty';
		$data['recordsFiltered'] = 0;
		$data['recordsTotal'] = 0;
		$data['draw'] = $this->input->post('draw');

		$where = "";
		$start = $this->input->post('start');
		$length = $this->input->post('length');

		$trip_status = $this->input->post('trip_status');

		$search = $this->input->post('search');
		if(isset($search['value']) && !empty($search['value']))
			$where .= " AND ( t.name = '" . $search['value'] . "' OR t.trip_number LIKE '%" . $search['value'] . "%' OR t.start_point LIKE '%" . $search['value'] . "%' OR t.end_point LIKE '%" . $search['value'] . "%' OR trk.name LIKE '%" . $search['value'] . "%' OR trl.name LIKE '%" . $search['value'] . "%' ) ";

		/*if($trip_status != "") {
			if($trip_status == "0")
				$where .= " AND IFNULL(m.devicename, '') = '' ";

			if($trip_status == "1")
				$where .= " AND IFNULL(m.devicename, '') != '' ";
		}*/

		if ($this->session->userdata('company')) {
			if ($this->session->userdata('company')['role'] == 2) {
				$company_id = $this->session->userdata('company')['company_id'];
				if(isset($company_id) && !empty($company_id) && $company_id != 0) {
					$sql = "SELECT t.id, t.trip_number, t.name, t.start_point, t.end_point, DATE_FORMAT(FROM_UNIXTIME(t.start_date), '%d-%m-%Y') AS start_date, DATE_FORMAT(FROM_UNIXTIME(t.expected_end_date), '%d-%m-%Y') AS end_date, trk.name AS truck_name, trl.name AS trailer_name, IFNULL(td.driver_id, '') AS driver_id, IFNULL(td.co_driver_id, '') AS co_driver_id, IFNULL(d.name, '') AS driver_name, IFNULL(cd.name, '') AS co_driver_name FROM " . $this->_web . $this->_mast . "trip AS t LEFT JOIN " . $this->_web . $this->_mast . "truck AS trk ON trk.id = t.truck_id LEFT JOIN " . $this->_web . $this->_mast . "trailer AS trl ON trl.id = t.trailer_id1 LEFT JOIN " . $this->_web . $this->_map . "trip_drivers AS td ON td.trip_id = t.id LEFT JOIN " . $this->_web . $this->_mast . "driver AS d ON d.id = IFNULL(td.driver_id, 0) LEFT JOIN " . $this->_web . $this->_mast . "driver AS cd ON cd.id = IFNULL(td.co_driver_id, 0) WHERE t.is_deleted = 0 AND t.company_id = '" . $company_id . "' " . $where;

					$getSqlCnt = $this->common->getRawQueryResult($sql);

					$sql .= " ORDER BY t.id DESC LIMIT " . $start . ", " . $length;

					$getSqlList = $this->common->getRawQueryResult($sql);
					
					if (count($getSqlList) > 0) {
						$data['draw'] = $this->input->post('draw');
						$data['recordsFiltered'] = count($getSqlCnt);
						$data['recordsTotal'] = count($getSqlCnt);
						$data['data'] = array();
						$no = $start;
						foreach ($getSqlList as $k) {
							$no++;
							$row = array();
							$row[] = $no;
							$row[] = !empty($k['trip_number']) ? 'T'.$k['trip_number'] : '-';
							$row[] = $k['name'];
							$row[] = $k['truck_name'];
							$row[] = $k['trailer_name'];
							$row[] = $k['start_point'];
							$row[] = $k['end_point'];
							$row[] = $k['start_date'];
							$row[] = $k['end_date'];

							$row[] = $k['driver_name'] ? $k['driver_name'] : '-';
							$row[] = $k['co_driver_name'] ? $k['co_driver_name'] : '-';

							$action = "";

							/*$action .= '<a href="portal_updatetrip.php?tid=<?php echo $trip['id'];?>" data-toggle="tooltip" title="Update Trip"><i class="fa fa-edit" style="font-size:18px;color:#008000;"></i></a>
							<a onclick="delete_id(<?php echo $trip['id'];?>)" data-toggle="tooltip" title="Delete Trip"><i class="fa fa-trash-o" style="font-size:18px;color:#FF0000;"></i></a>  
							<a href="portal_assigndriver.php?tid=<?php echo $trip['id'];?>" data-toggle="tooltip" title="Assign Driver"><i class="fa fa-user-plus" style="font-size:18px;color:#008000;"></i></a>
							<a href="portal_assigncodriver.php?tid=<?php echo $trip['id'];?>" data-toggle="tooltip" title="Assign Co-Driver"><i class="fa fa-user-plus" style="font-size:18px; color:#2444ad;"></i></a>
							<a href="#" data-toggle="modal" data-target="#myModal<?php echo $trip['id'];?>" title="Checklist"><i class="fa fa-list" style="font-size:18px;color:#008000;"></i></a>*/

							$action .= '<a href="#" data-toggle="tooltip" title="Update Trip"><i class="fa fa-edit" style="font-size:18px; color:#001000;"></i></a>';
                            $action .= '<a data-toggle="tooltip" title="Delete Trip" class="px-1 update_trip_status" data-trip_status="1" data-trip_id="'.$k['id'].'" data-company_id="'.$company_id.'"><i class="fa fa-eye-slash" style="font-size:18px; color:#ff0000;"></i></a>';
                            /*$action .= '<a href="#" data-toggle="tooltip" title="Assign Driver"><i class="fa fa-user-plus" style="font-size:18px; color:#008000;"></i></a>';
							$action .= '<a href="#" data-toggle="tooltip" title="Assign Co-Driver"><i class="fa fa-user-plus" style="font-size:18px; color:#2444ad;"></i></a>';*/
							$action .= '<a data-toggle="tooltip" title="Assign Driver" class="px-1 assign_driver_codriver" data-trip_id="'.$k['id'].'" data-company_id="'.$company_id.'" data-driver_id="'.$k['driver_id'].'"  data-co_driver_id="'.$k['co_driver_id'].'" data-trip_number="'.(!empty($k['trip_number']) ? 'T'.$k['trip_number'] : '-').'"><i class="fa fa-user-plus" style="font-size:18px; color:#008000;"></i></a>';
							//$action .= '<a href="#" data-toggle="modal" data-target="#myModal" title="Checklist"><i class="fa fa-list" style="font-size:18px; color:#008000;"></i></a>';

                            $row[] = $action;
							
							$data['data'][] = $row;
						}
						$data['status'] = 'success';
					} else
						$data['status'] = 'empty';
				} else
					$data['status'] = 'Company info not found';
			} else 
				$data['status'] = 'User role not matched to this class';
		} else 
			$data['status'] = 'Session not found';

		echo json_encode($data);
	}

	/**
	 *  Delete Trip
	 */
	public function deleteTrip() {
		$data = array('is_deleted' => $this->input->post('status'));
		$cond = array('id' => $this->input->post('id'));
		$table_name = $this->_web.$this->_mast.'trip';
		$this->common->update($data, $cond, $table_name);

		if($this->input->post('status') == '0')
			$json_data = array('status' => true, 'status_text' => 'Trip details activated');
		else
			$json_data = array('status' => true, 'status_text' => 'Trip details deleted');

		echo json_encode($json_data);
	}

	/**
	 *	Get User List for Assign / Unassign Driver & Co-Driver for Trip
	 */
	public function getUserForAssignDriverCoDriver() {
		$trip_id = $this->input->post('trip_id');
		$company_id = $this->input->post('company_id');

		$driver_sql = "SELECT id, name FROM " . $this->_web . $this->_mast . "driver WHERE is_deleted = 0 AND company_id = " . $company_id . " AND id NOT IN (SELECT co_driver_id FROM " . $this->_web . $this->_map . "trip_drivers WHERE company_id = " . $company_id . " AND trip_id = " . $trip_id . ") ORDER BY id ASC";

		$co_driver_sql = "SELECT id, name FROM " . $this->_web . $this->_mast . "driver WHERE is_deleted = 0 AND company_id = " . $company_id . " AND id NOT IN (SELECT driver_id FROM " . $this->_web . $this->_map . "trip_drivers WHERE company_id = " . $company_id . " AND trip_id = " . $trip_id . ") ORDER BY id ASC";

		$assigned_driver_sql = "SELECT IFNULL(driver_id, '') AS driver_id, IFNULL(co_driver_id, '') AS co_driver_id FROM " . $this->_web . $this->_map . "trip_drivers WHERE company_id = " . $company_id . " AND trip_id = " . $trip_id . " ";

		$getUsersForDriver = $this->common->getRawQueryResult($driver_sql);
		$getUsersForCoDriver = $this->common->getRawQueryResult($co_driver_sql);
		$getAssignedDriver = $this->common->getRawQueryResult($assigned_driver_sql);

		echo json_encode(array(
			'status' => true,
			'driver_users' => count($getUsersForDriver) > 0 ? $getUsersForDriver : [],
			'co_driver_users' => count($getUsersForCoDriver) > 0 ? $getUsersForCoDriver : [],
			'assigned_driver' => count($getAssignedDriver) > 0 ? $getAssignedDriver[0]['driver_id'] : "",
			'assigned_co_driver' => count($getAssignedDriver) > 0 ? $getAssignedDriver[0]['co_driver_id'] : ""
		));
	}

	/**
	 *	Save / Update Driver & Co-Driver for Trip
	 */
	public function getSaveUpdateDriverCoDriver() {
		$json_data = array();
		$json_data['status'] = true;
		$trip_id = $this->input->post('trip_id');
		$company_id = $this->input->post('company_id');
		$driver_id = $this->input->post('driver_id');
		$co_driver_id = $this->input->post('co_driver_id');

		$table_name = $this->_web . $this->_map . "trip_drivers";
		$field = 'id';
		$flag = 1; // 0 - Row Array, 1 - Result Array
		$cond = array('trip_id' => $trip_id);
		$get_trip_drivers_count = $this->common->fetchData($table_name, $field, $cond, '', $flag);

		if(count($get_trip_drivers_count) > 0) {
			$data = array('driver_id' => $driver_id, 'co_driver_id' => $co_driver_id, 'update_time' => time());
			$cond = array('trip_id' => $trip_id);
			$this->common->update($data, $cond, $table_name);
			
			$json_data['status_text'] = 'Driver & Co-driver updated for the trip details';
		} else {
			$data = array(
				'trip_id' => $trip_id,
				'company_id' => $company_id,
				'driver_id' => $driver_id,
				'co_driver_id' => $co_driver_id,
				'assign_time' => time()
			);
			$this->common->insert($data, $table_name);
			
			$json_data['status_text'] = 'Driver & Co-driver added for the trip details';
		}

		echo json_encode($json_data);
	}
	/********************************************/
	/**********************		TRIP MODULE ENDS	 **********************/
	/********************************************/





	/********************************************/
	/**********************		DEVELOPER TESTING FUNCTION STARTS	 **********************/
	/********************************************/	
	public function testing() {
		echo enc(42) . '<br>';
		echo dec(enc(42)) . '<br>';
		echo base64_encode(42) . '<br>';
		echo base64_decode(base64_encode(42)) . '<br>';
		echo get_encrypt_decrypt(42) . '<br>';
		echo get_encrypt_decrypt(get_encrypt_decrypt(42)) . '<br>';
		echo strtolower(encode(42)) . '<br>';
		echo decode(strtolower(encode(42))) . '<br>';
		exit;
	}
	/********************************************/
	/**********************		DEVELOPER TESTING FUNCTION ENDS	 **********************/
	/********************************************/	
}