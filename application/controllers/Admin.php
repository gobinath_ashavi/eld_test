<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 *	Routes Alias Name : admin
 *	Session Name : admin
 */
class Admin extends CI_Controller
{
	public function __construct() {
		parent::__construct();
		$this->_web = $this->config->item('TABLE_WEB_PREFIX');
        $this->_map = $this->config->item('TABLE_MAP_PREFIX');
        $this->_mast = $this->config->item('TABLE_MAST_PREFIX');
	}

	/**
	 *  Super Admin Home / Dashboard
	 */
	public function home() {
		if ($this->session->userdata('admin')) {
			if ($this->session->userdata('admin')['role'] == '1') {
				// super admin
				$data = array();
				$this->load->view('admin/home', $data);
			}
		} else
			redirect('auth/signin');
	}
}