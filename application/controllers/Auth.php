<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *	Routes Alias Name : auth
 *	Session Name : session_user_details
 */
class Auth extends CI_Controller 
{
	public function __construct() {
        parent::__construct();
        $this->_web = $this->config->item('TABLE_WEB_PREFIX');
        $this->_map = $this->config->item('TABLE_MAP_PREFIX');
        $this->_mast = $this->config->item('TABLE_MAST_PREFIX');
    }

	public function index() {
		$this->load->view('signin');
	}

	/*************************	 SIGNIN RELATED FUNCTIONALITY STARTS	***********************/
	/**
	 *	Render Login Page
	 */
	public function signin() {
		if($this->session->userdata('session_user_details')) {
			if ($this->session->userdata('session_user_details')['role'] == '1')
				// Admin
				redirect('admin/home');
			elseif($this->session->userdata('session_user_details')['role'] == '2')
				// Company
				redirect('company/home'); 
			elseif($this->session->userdata('session_user_details')['role'] == '3')
				// User
				redirect('user/home');
			else
				redirect('auth/signin');
		} else
			$this->load->view('signin');
	}

	/**
	 *	Render Home Page
	 */
	public function home() {
		if($this->session->userdata('session_user_details')) {
			if ($this->session->userdata('session_user_details')['role'] == '1')
				// Admin
				redirect('admin/home');
			elseif($this->session->userdata('session_user_details')['role'] == '2')
				// Company
				redirect('company/home'); 
			elseif($this->session->userdata('session_user_details')['role'] == '3')
				// User
				redirect('user/home');
			else
				redirect('auth/signin'); 
		} else
			redirect('auth/signin'); 
	}

	/**
	 *	Authenticate User Data
	 */
	public function authenticate() {
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		if($email && $password) {
			$field = '*';
			$limit = 1;
			$flag = 0; // 0 - Row Array, 1 - Result Array

			$table_name_1 = $this->_web . $this->_mast . 'auth_user';
			$cond_1 = array('username' => $email, 'password' => $password);
			$auth_user_row = $this->common->fetchData($table_name_1, $field, $cond_1, $limit, $flag);
			if(!empty($auth_user_row) && count($auth_user_row) > 0) {
				if($auth_user_row['is_deleted'] == 0) {
					$auth_user_data = array(
						'id'     		=> $auth_user_row['id'],
						'cid'			=> "",
						'name' 			=> "",
						'username'  	=> $auth_user_row['username'],
						'role'			=> $auth_user_row['usertype'],
						'usertype'		=> $auth_user_row['usertype'],
						'email'     	=> (isset($auth_user_row['email']) && !empty($auth_user_row['email']) ? $auth_user_row['email'] : ""),
						'activeuid'		=> $auth_user_row['id'],
						'timezone'		=> "",
						'lname' 		=> "",
						'sform'			=> "",
						'speed_unit'	=> "",
						'fuel_unit'		=> "",
						'company_id'	=> "",
						'logged_in' 	=> true
					);
					$this->session->set_userdata('session_user_details', $auth_user_data);
					
					$session_name = get_session_name_by_role($auth_user_row['usertype']);

					if($auth_user_row['usertype'] == 1)
						$this->session->set_userdata($session_name, $auth_user_data);

					if($auth_user_row['usertype'] == 2) {
						$company_sql = 'SELECT c.lname, au.username, c.id AS cid, c.name As name, tz.timezone, tz.sform, c.speed_unit,c.fuel_unit FROM ' . $this->_web . $this->_mast . 'auth_user AS au JOIN ' . $this->_web . $this->_mast . 'company AS c ON au.id = c.user_id JOIN ' . $this->_web . $this->_mast . 'timezonelist as tz ON tz.id=c.timezone WHERE au.id = ' . $auth_user_row['id'] . ' AND c.status = 0';

						$company_row = $this->common->getRawQueryResult($company_sql);
						if(count($company_row) > 0) {
							$company_row = $company_row[0];
							$company_data = array(
								'id'     		=> $auth_user_row['id'],
								'cid'			=> $company_row['cid'],
								'name' 			=> $company_row['lname'],
								'username'  	=> $company_row['username'],
								'role'			=> 2,
								'usertype'		=> 2,
								'email'     	=> "",
								'activeuid'		=> $auth_user_row['id'],
								'timezone'		=> $company_row['timezone'],
								'lname' 		=> $company_row['lname'],
								'sform'			=> $company_row['sform'],
								'speed_unit'	=> $company_row['speed_unit'],
								'fuel_unit'		=> $company_row['fuel_unit'],
								'company_id'	=> $company_row['cid'],
								'logged_in' 	=> true
							);

							$this->session->set_userdata($session_name, $company_data);
						}
					} else if($auth_user_row['usertype'] == 3) {
						$user_sql = 'SELECT s.company_id AS cid, s.name, s.lname, au.username, c.name AS cname, c.speed_unit, c.fuel_unit, tz.sform, tz.timezone, ur.modules, ur.sub_modules FROM ' . $this->_web . $this->_mast . 'user AS s JOIN ' . $this->_web . $this->_mast . 'auth_user AS au ON au.id = IFNULL(s.user_id, 0) JOIN ' . $this->_web . $this->_mast . 'company AS c ON c.id = s.company_id JOIN ' . $this->_web . $this->_mast . 'timezonelist AS tz ON tz.id = c.timezone JOIN ' . $this->_web . $this->_map . 'user_role AS ur ON ur.id = IFNULL(s.user_role, 0) WHERE s.id = ' . $auth_user_row['id'] . ' AND s.status = 0 AND s.is_deleted = 0';

						$user_row = $this->common->getRawQueryResult($user_sql);
						if(count($user_row) > 0) {
							$user_row = $user_row[0];
							$user_data = array(
								'id'     		=> $auth_user_row['id'],
								'cid'			=> $user_row['cid'],
								'name' 			=> $user_row['sub_modules'],
								'username'  	=> $user_row['username'],
								'role'			=> 3,
								'usertype'		=> 3,
								'email'     	=> "",
								'activeuid'		=> $auth_user_row['id'],
								'timezone'		=> $user_row['timezone'],
								'lname' 		=> $user_row['lname'],
								'sform'			=> $user_row['sform'],
								'speed_unit'	=> $user_row['speed_unit'],
								'fuel_unit'		=> $user_row['fuel_unit'],
								'company_id'	=> $user_row['cid'],
								'modules'		=> $user_row['modules'],
								'sub_modules'	=> $user_row['sub_modules'],
								'logged_in' 	=> true
							);

							$this->session->set_userdata($session_name, $user_data);
						}
					}					

					$json_data = array(
						'status' 		=> true,
						'status_text' 	=> 'Welcome ' . $auth_user_row['username'],
						'user_data'		=> $auth_user_data
					);
					echo json_encode($json_data);
				} else if($auth_user_data['is_deleted'] == 1) {
					$json_data = array(
						'status' 		=> false,
						'status_text' 	=> 'Your account is inactive. Please contact admin to activate your account.',
						'user_data'		=> array()
					);
					echo json_encode($json_data);
				}
			} else {
				$json_data = array(
					'status' 		=> false,
					'status_text' 	=> 'Email / Password not valid.',
					'user_data'		=> false
				);
				echo json_encode($json_data);
			}
		} else {
			$json_data = array(
				'status' 		=> false,
				'status_text' 	=> 'Email / Password should not empty.',
				'user_data'		=> false
			);
			echo json_encode($json_data);
		}
	}
	/*************************	 SIGNIN RELATED FUNCTIONALITY ENDS	***********************/

	/*************************	 SIGNUP RELATED FUNCTIONALITY STARTS	***********************/
	/**
	 *	Render Registration Page
	 */
	public function signup() {
		$this->load->view('signup');
	}

	/**
	 *	Get User Present or Not
	 */
	protected function checkEmail($email = '') {
		$table_name = 'users';
		$field = '*';
		$limit = 1;
		$flag = 0; // 0 - Row Array, 1 - Result Array
		$cond = array('email' => $email);
		$user_row = $this->common->fetchData($table_name, $field, $cond, $limit, 0);
		return $user_row;
	}

	/**
	 *	Create New User Information
	 */
	public function newUser() {
		$first_name = $this->input->post('first_name');
		$last_name = $this->input->post('last_name');
		$driver_license = $this->input->post('driver_license');
		$company_name = $this->input->post('company_name');
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$status = 'active';
		// $role = $this->input->post('role');
		$role = '3';
		$registered_on = date('Y-m-d H:i:s');
		$password = md5_password($password); //md5(md5($password).md5('12345678'));
		if($email) {
			$row = $this->checkEmail($email);
			if($row && count($row) > 0) {
				$json_data = array(
					'status' 		=> false,
					'status_text' 	=> 'Email already registered.'
				);
				echo json_encode($json_data);
			} else {
				$data = array(
					'first_name' 	=> $first_name,
					'last_name' 	=> $last_name,
					'driver_license' 	=> $driver_license,
					'company_name' 	=> $company_name,
					'user_name' 	=> $first_name.($last_name ? '.'.$last_name : ''),
					'email' 		=> $email,
					'password_hash' => $password,
					'password_salt' => $this->config->item('PASSWORD_SALT_KEY'), //'12345678',
					'status' 		=> $status,
					'role' 			=> $role,
					//'role' 		=> get_user_role_id($role),
					'registered_on' => $registered_on
				);
				$table_name = 'users';
				$this->common->insert($data, $table_name);
				$json_data = array(
					'status' 		=> true,
					'status_text' 	=> 'Registration completed.'
				);
				echo json_encode($json_data);
			}
		}
	}
	/*************************	 SIGNUP RELATED FUNCTIONALITY ENDS	***********************/

	/*************************	 LOGOUT FUNCTIONALITY STARTS	***********************/
	public function logout() {
		$this->session->unset_userdata('session_user_details');
		
		$this->session->unset_userdata('admin');
		$this->session->unset_userdata('company');
		$this->session->unset_userdata('user');

		$this->session->sess_destroy();
		redirect('auth/signin');
	}
	/*************************	 LOGOUT FUNCTIONALITY ENDS	***********************/
	
	/*************************	 FORGOT PASSWORD FUNCTIONALITY ENDS	***********************/
	/**
	 *	Update User Reset Password
	 */
	public function checkUserEmailForForgotPassword() {
		$email = $this->input->post('email'); // 'raja.rko79@gmail.com'; 
		if($email) {
			$row = $this->checkEmail($email);
			if($row && count($row) > 0) {
				$arr['template_name'] = 'forgot_password';
				$arr['email'] = $email;
				$arr['user_id'] = isset($row['id']) ? $row['id'] : 0;
				$arr['custom_url'] = get_site_url().'auth/resetPassword/'.enc($row['id']);
				$arr['user_name'] = isset($row['first_name']) ? $row['first_name'] . ($row['last_name'] ? ' ' . $row['last_name'] : '') : '';
				$this->common_email->getSendEmail($arr);
				$json_data = array(
					'status' 		=> true,
					'status_text' 	=> 'Reset password link sent successfully to your email. Please check your email.'
				);
				echo json_encode($json_data);
			} else {
				$json_data = array(
					'status' 		=> false,
					'status_text' 	=> 'Email not exist. Please enter the valid registered email.'
				);
				echo json_encode($json_data);
			}
		} else {
			$json_data = array(
				'status' 		=> false,
				'status_text' 	=> 'Email should not empty.'
			);
			echo json_encode($json_data);
		}
	}
	/*************************	 FORGOT PASSWORD FUNCTIONALITY ENDS	***********************/

	/*************************	 RESET PASSWORD RELATED FUNCTIONALITY STARTS	***********************/
	/**
	 *	Render Reset Password Page
	 */
	public function resetPassword($enc_user_id = '') {
		$enc_user_id  = ($this->uri->segment('3') ? $this->uri->segment('3') : '') . ($this->uri->segment('4') ? $this->uri->segment('4') : '') . ($this->uri->segment('5') ? $this->uri->segment('5') : '') . ($this->uri->segment('6') ? $this->uri->segment('6') : '') . ($this->uri->segment('7') ? $this->uri->segment('7') : '') . ($this->uri->segment('8') ? $this->uri->segment('8') : '');
		//echo $enc_user_id . ' ===== <br/>';
		$this->load->view('reset_password', array(
			'enc_user_id' => $enc_user_id
		));
	}

	/**
	 *	Update User Reset Password
	 */
	public function updateResetPassword() {
		$new_password = $this->input->post('new_password');
		$new_password = md5_password($new_password); //md5(md5($password).md5('12345678'));
		$enc_user_id = $this->input->post('enc_user_id');
		$user_id = isset($enc_user_id) ? dec($enc_user_id) : 0;
		if(isset($user_id) && $isset($new_password)) {
			$data['password_salt'] = $this->config->item('PASSWORD_SALT_KEY');
			$data['password_hash'] = $new_password;
			$cond['id'] = $user_id;			
			$table_name = 'users';
			$this->common->update($data, $cond, $table_name);
			$arr['template_name'] = 'reset_password';
			$arr['email'] = '';
			$arr['user_id'] = isset($user_id) ? $user_id : 0;
			$arr['custom_url'] = '';
			$arr['user_name'] = '';
			$this->common_email->getSendEmail($arr);
			$json_data = array(
				'status' 		=> true,
				'status_text' 	=> 'Reset password updated successfully. Please login to the site.'
			);
			echo json_encode($json_data);
		} else {
			$json_data = array(
				'status' 		=> false,
				'status_text' 	=> 'Password / User token is not valid. Please check the valid url which is mentioned in your email for resetting your password.'
			);
			echo json_encode($json_data);
		}
	}
	/*************************	 RESET PASSWORD RELATED FUNCTIONALITY ENDS	***********************/
}