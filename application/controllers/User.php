<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 *	Routes Alias Name : user
 *	Session Name : user
 */
class User extends CI_Controller
{
	public function __construct() {
		parent::__construct();
		$this->_web = $this->config->item('TABLE_WEB_PREFIX');
        $this->_map = $this->config->item('TABLE_MAP_PREFIX');
        $this->_mast = $this->config->item('TABLE_MAST_PREFIX');
	}

	/**
	 *  User Home / Dashboard
	 */
	public function home() {
		if ($this->session->userdata('user')) {
			if ($this->session->userdata('user')['role'] == 3) {
				// user
				$data = array();
				$this->load->view('user/home', $data);
			}
		} else
			redirect('auth/signin');
	}
}