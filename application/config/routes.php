<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|☺
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller'] = 'auth/signin';

/***********    AUTH CONTROLLER ROUTES STARTS    ***********/
$route['/signin'] = 'auth';
$route['/signin'] = 'auth/signin';
$route['/signup'] = 'auth/signup';
$route['/resetPassword/{:id}'] = 'auth/resetPassword/{:id}';
/***********    AUTH CONTROLLER ROUTES ENDS    ***********/

/***********    SUPER ADMIN CONTROLLER ROUTES STARTS    ***********/
/***********    SUPER ADMIN CONTROLLER ROUTES ENDS    ***********/

/***********    COMPANY CONTROLLER ROUTES STARTS    ***********/
$route['/company/driver/view'] = 'company/viewDriver';
$route['/company/driver/list'] = 'company/getDriverList';
$route['/company/driver/delete'] = 'company/deleteDriver';

$route['/company/user/view'] = 'company/viewUser';
$route['/company/user/list'] = 'company/getUserList';
$route['/company/user/delete'] = 'company/deleteUser';

$route['/company/role/view'] = 'company/viewRole';
$route['/company/role/list'] = 'company/getRoleList';
$route['/company/role/delete'] = 'company/deleteRole';

$route['/company/switch/user'] = 'company/switchUser';

$route['/company/device/assigned/view'] = 'company/viewAssignedDevice';
$route['/company/device/assigned/list'] = 'company/getAssignedDeviceList';
/***********    COMPANY CONTROLLER ROUTES ENDS    ***********/

/***********    USER CONTROLLER ROUTES STARTS    ***********/
/***********    USER CONTROLLER ROUTES ENDS    ***********/

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;