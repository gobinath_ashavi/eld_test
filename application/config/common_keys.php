<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['PASSWORD_SALT_KEY'] = '12345678';

$config['COMMON_SALT_KEY'] = 'beast_incarnate';

$config['PROJECT_TITLE_FOR_BROWSER_TAB'] = 'AG';

$config['PROJECT_TITLE'] = 'Ashaviglobal';

$config['ENCRYPT_METHOD'] = 'AES-128-CBC';

$config['SEPARATOR'] = ':';

$config['TABLE_APP_PREFIX'] = 'app_';

$config['TABLE_WEB_PREFIX'] = 'web_';

$config['TABLE_MAP_PREFIX'] = 'map_';

$config['TABLE_MAST_PREFIX'] = 'mast_';

$config['TABLE_TRANS_PREFIX'] = 'trans_';