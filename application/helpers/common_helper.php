<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//	1 - Super Admin, 2 - Company, 3 - User

if (!function_exists('get_user_role_name')) {
	function get_user_role_name($role_id = '')
	{
		switch ($role_id) {
			case 1:
				$role_name = 'Super Admin';
				break;

			case 2:
				$role_name = 'Company';
				break;

			case 3:
				$role_name = 'User';
				break;

			default:
				$role_name = 'User';
				break;
		}

		return $role_name;
	}
}

if (!function_exists('get_user_role_id')) {
	function get_user_role_id($role_name = '')
	{
		switch ($role_name) {
			case 'Super Admin':
				$role_id = 1;
				break;

			case 'Company':
				$role_id = 2;
				break;

			case 'User':
				$role_id = 3;
				break;

			default:
				$role_id = 3;
				break;
		}

		return $role_id;
	}
}

if (!function_exists('get_site_url')) {
	function get_site_url()
	{
		if (isset($_SERVER['HTTPS']))
			$protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
		else
			$protocol = 'http';

		//return $protocol . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		return $protocol . "://" . $_SERVER['HTTP_HOST'] . '/eld/';
	}
}

$__COMMON_SALT_KEY = 'beast_incarnate';
$__PASSWORD_SALT_KEY = '12345678';
$__ENCRYPT_METHOD = 'AES-128-CBC';
$__SEPARATOR = ':';

if (!function_exists('enc')) {
	function enc($string = '')
	{
		// MD5 + SALT Encrypt
		$CI =& get_instance();
		$__COMMON_SALT_KEY = get_salt_key(); //$CI->config->item('COMMON_SALT_KEY');
		$__ENCRYPT_METHOD = $CI->config->item('ENCRYPT_METHOD');
		$__SEPARATOR = $CI->config->item('SEPARATOR');

		/*$encryption_key = base64_decode($__COMMON_SALT_KEY);
		$iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
		$encrypted = openssl_encrypt($string, 'aes-256-cbc', $encryption_key, 0, $iv);
		return base64_encode($encrypted . '::' . $iv);*/

		$iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($__ENCRYPT_METHOD));
		return base64_encode(openssl_encrypt($string, $__ENCRYPT_METHOD, $__COMMON_SALT_KEY, 0, $iv) . $__SEPARATOR . base64_encode($iv));
	}
}

if (!function_exists('dec')) {
	function dec($string = '')
	{
		// MD5 + SALT Decrypt
		$CI =& get_instance();
		$__COMMON_SALT_KEY = get_salt_key(); //$CI->config->item('COMMON_SALT_KEY');
		$__ENCRYPT_METHOD = $CI->config->item('ENCRYPT_METHOD');
		$__SEPARATOR = $CI->config->item('SEPARATOR');

		/*$encryption_key = base64_decode($__COMMON_SALT_KEY);
		list($encrypted_data, $iv) = array_pad(explode('::', base64_decode($string), 2), 2, null);
		return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);*/

		$parts = explode($__SEPARATOR, base64_decode($string));
		// $parts[0] = encrypted data
		// $parts[1] = initialization vector
		return openssl_decrypt($parts[0], $__ENCRYPT_METHOD, $__COMMON_SALT_KEY, 0, base64_decode($parts[1]));
	}
}

if (!function_exists('md5_password')) {
	function md5_password($password = '')
	{
		// MD5 + SALT Encrypt
		$CI =& get_instance();
		$__PASSWORD_SALT_KEY = $CI->config->item('PASSWORD_SALT_KEY');

		return md5(md5($password) . md5($__PASSWORD_SALT_KEY));
	}
}

if (!function_exists('random_string_generator')) {
	function random_string_generator($length)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
}

if (!function_exists('date_time_difference')) {
	function date_time_difference($start_date = null, $end_date = null, $diff = "minutes")
	{
		$start_date = new DateTime($start_date);
		$end_date = $start_date->diff(new DateTime($end_date)); // date now
		switch ($diff) {
			case 'seconds':
				return $end_date->s;
				break;
			case 'minutes':
				return $end_date->i;
				break;
			case 'hours':
				return $end_date->h;
				break;
			case 'days':
				return $end_date->d;
				break;
			default:
				# code...
				break;
		}
	}
}

if (!function_exists('get_session_name_by_role')) {
	function get_session_name_by_role($role_id = '')
	{
		switch ($role_id) {
			case 1:
				$session_name = 'admin';
				break;

			case 2:
				$session_name = 'company';
				break;

			case 3:
				$session_name = 'user';
				break;

			default:
				$session_name = 'session_user_details';
				break;
		}

		return $session_name;
	}
}

if (!function_exists('get_encrypt_decrypt')) {
	function get_encrypt_decrypt($num = 0) {
	    return (((0x0000FFFF & $num) << 16) + ((0xFFFF0000 & $num) >> 16));
	}
}

if (!function_exists('encode')) {
	function encode($id = '') {
		$id_str = (string) $id;
		$offset = rand(0, 9);
		$encoded = chr(79 + $offset);
		for ($i = 0, $len = strlen($id_str); $i < $len; ++$i) {
			$encoded .= chr(65 + $id_str[$i] + $offset);
		}
		return $encoded;
	}
}

if (!function_exists('decode')) {
	function decode($encoded = '') {
		$offset = ord($encoded[0]) - 79;
		$encoded = substr($encoded, 1);
		for ($i = 0, $len = strlen($encoded); $i < $len; ++$i) {
			$encoded[$i] = ord($encoded[$i]) - $offset - 65;
		}
		return (int) $encoded;
	}
}

if (!function_exists('get_salt_key')) {
	function get_salt_key() {
		$CI =& get_instance();
		return $CI->config->item('COMMON_SALT_KEY') ? $CI->config->item('COMMON_SALT_KEY') : "beast_incarnate";
	}
}

if (!function_exists('convert_date')) {
	function convert_date($dt = '') {
		$current_date = $dt ? $dt : date('Y-m-d h:m:i A');
		$convert_date = new DateTime($current_date);
		$convert_date->setTimezone(new DateTimeZone('GMT'));
		$format_date = $convert_date->format("Y-m-d h:m:i A");

		return $format_date;
	}
}

if (!function_exists('get_project_name_for_browser_tab')) {
	function get_project_name_for_browser_tab() {
		$CI =& get_instance();
		return $CI->config->item('PROJECT_TITLE_FOR_BROWSER_TAB') ? $CI->config->item('PROJECT_TITLE_FOR_BROWSER_TAB') : "AG";	
	}
}